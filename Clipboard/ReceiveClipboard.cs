﻿using Event;
using ObjectSerializable;
using System;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;

namespace ClipboardControl
{
    public delegate void ReceiveEventclipboard(object sender, MyEventArgs e);

    public class ReceiveClipboard
    {
        public event ReceiveEventclipboard ErrorReceive;
        public event ReceiveEventclipboard FinishReceive;
        public event SendEventHandler UpdateProgressBarReceive;

        private Socket _clipboard;
        private int BUFFER_SIZE = 1024 * 1024;
        private const string _path = @"C:\temp\";

        #region ReceiveClipboard
        public ReceiveClipboard(Socket c)
        {
            _clipboard = c;

            StartReceive();
        }
        #endregion

        #region StartReceive
        private void StartReceive()
        {
            try
            {
                StateClipboardObject stateClipboard = new StateClipboardObject();

                // Begins to asynchronously receive the length of the _clipboard object
                _clipboard.BeginReceive(
                    stateClipboard.objLen,          // An array of type Byt for received data 
                    0,                          // The zero-based position in the buffer  
                    4,                          // The number of bytes to receive 
                    SocketFlags.None,           // Specifies send and receive behaviors 
                    new AsyncCallback(ReceiveLenghtClipboardCallback),//An AsyncCallback delegate 
                    stateClipboard            // Specifies infomation for receive operation 
                    );
            }
            catch (Exception e)
            {
                OnErrorReceive(new MyEventArgs(null));
            }
        } 
        #endregion

        #region ReceiveLenghtClipboardCallback
        private void ReceiveLenghtClipboardCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the _clipboard socket
                // from the asynchronous state object.
                StateClipboardObject stateClipboardObject = (StateClipboardObject)ar.AsyncState;

                Debug.Print("Receive clibboard");

                // Read data from the client socket. 
                int bytesRead = _clipboard.EndReceive(ar);

                if (bytesRead > 0)
                {
                    Debug.Print("Length: " + bytesRead);
                    // length received
                    stateClipboardObject.toReceive = BitConverter.ToInt32(stateClipboardObject.objLen, 0);
                    // instanziate new buffer of lenght reveived before
                    stateClipboardObject.buffer = new byte[stateClipboardObject.toReceive];

                    // Begins to asynchronously receive data 
                    _clipboard.BeginReceive(
                        stateClipboardObject.buffer,     // An array of type Byt for received data 
                        0,                              // The zero-based position in the buffer  
                        stateClipboardObject.buffer.Length,  // The number of bytes to receive 
                        SocketFlags.None,           // Specifies send and receive behaviors 
                        new AsyncCallback(ReceiveObjectClipboardCallback),//An AsyncCallback delegate 
                        stateClipboardObject            // Specifies infomation for receive operation 
                        );

                }
                else
                {
                    stateClipboardObject.fileStream.Close();
                    OnErrorReceive(new MyEventArgs(null));
                }
            }
            catch (Exception e)
            {
                OnErrorReceive(new MyEventArgs(null));
            }
        }
        #endregion

        #region ReceiveObjectClipboardCallback
        private void ReceiveObjectClipboardCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the _clipboard socket
                // from the asynchronous state object.
                StateClipboardObject stateClipboardObject = (StateClipboardObject)ar.AsyncState;

                // Read data from the client socket. 
                int bytesRead = _clipboard.EndReceive(ar);

                if (bytesRead > 0)
                {
                    stateClipboardObject.toReceive -= bytesRead;                   

                    if (stateClipboardObject.toReceive == 0)
                    {
                        stateClipboardObject.totalReceived += bytesRead;
                        // if there's the last chunk
                        if (stateClipboardObject.ms != null)
                        {
                            stateClipboardObject.ms.Write(stateClipboardObject.buffer, 0, bytesRead);
                        }

                        Debug.Print("Obj: " + stateClipboardObject.totalReceived);
                        if (stateClipboardObject.ms != null)
                            EventClipboardReceived(stateClipboardObject.ms.ToArray(), stateClipboardObject);
                        else
                            EventClipboardReceived(stateClipboardObject.buffer, stateClipboardObject);
                    }
                    else
                    {
                        stateClipboardObject.totalReceived += bytesRead;
                        if(stateClipboardObject.ms == null)
                            stateClipboardObject.ms = new MemoryStream();
                        // Transfer the imcomplete object into the steam.
                        stateClipboardObject.ms.Write(stateClipboardObject.buffer, 0, bytesRead);
                        // Create a new buffer ready for new remaining incoming bytes.
                        stateClipboardObject.buffer = new byte[stateClipboardObject.toReceive];

                        _clipboard.BeginReceive(
                            stateClipboardObject.buffer,            // An array of type Byt for received data 
                            0,                                  // The zero-based position in the buffer  
                            stateClipboardObject.buffer.Length,        // The number of bytes to receive 
                            SocketFlags.None,                   // Specifies send and receive behaviors 
                            new AsyncCallback(ReceiveObjectClipboardCallback), //An AsyncCallback delegate 
                            stateClipboardObject                    // Specifies infomation for receive operation 
                            );
                    }
                }
                else
                {
                    OnErrorReceive(new MyEventArgs(null));
                }
            }
            catch (Exception e)
            {
                OnErrorReceive(new MyEventArgs(null));
            }
        }
        #endregion

        #region EventClipboardReceived
        private async void EventClipboardReceived(byte[] buffer, StateClipboardObject stateCliboardObject)
        {
            try
            {
                ClipboardItem eventClipboard = (ClipboardItem)DeserializeFromBytes(buffer);

                switch (eventClipboard.Type)
                {
                    case "text":
                        SetClipboard.SetText(eventClipboard);
                        break;
                    case "image":
                        SetClipboard.SetImage(eventClipboard);
                        break;
                    case "audio":
                        SetClipboard.SetAudio(eventClipboard);
                        break;
                    case "file":
                        // the list of file that it has received
                        stateCliboardObject.fileList = ((ClipboardFileList)eventClipboard).FileList;

                        bool success = await ReceiveAllFiles(stateCliboardObject);

                        if (!success)
                            OnErrorReceive(new MyEventArgs("Errore ricezione file"));

                        // set only name of the file.
                        SetClipboard.SetFile(eventClipboard);
                        break;
                }

                #region NotFile
                OnFinishReceive(new MyEventArgs("ricezione completata"));
                StartReceive();
                #endregion
            }
            catch (Exception e)
            {
                OnErrorReceive(new MyEventArgs(null));
            }
        }
        #endregion

        #region
        private async Task<bool> ReceiveAllFiles(StateClipboardObject stateCliboardObject)
        {
            bool temp = await Task.Run(() =>
            {
                foreach (ClipboardFile cf in stateCliboardObject.fileList)
                {
                    bool success = ReceiveSingleFile(cf);

                    if (!success)
                        return false;
                }
                return true;
            });
            return temp;
        }
        #endregion

        #region ReceiveSingleFile
        private bool ReceiveSingleFile(ClipboardFile clipoardFile)
        {
            FileStream file = null;
            try
            {
                file = CreateFile(clipoardFile);
                long toReceive = clipoardFile.FileLen;

                Debug.Print("Dimesione file: " + toReceive + " bytes" + " of :" + clipoardFile.Name);

                long byteRicevuti = 0;
                long originalDimFile = toReceive;
                /* loop until it isn't sent all file */
                byte[] buffer = new byte[BUFFER_SIZE];

                while (true)
                {
                    int numBytesChunkReceived = 0;

                    if (toReceive < BUFFER_SIZE)
                        buffer = new byte[toReceive];

                    while (numBytesChunkReceived < buffer.Length)
                    {

                        int bytesReceived = _clipboard.Receive(buffer, numBytesChunkReceived, buffer.Length - numBytesChunkReceived, SocketFlags.None);

                        if (bytesReceived == 0)
                            break;
                        numBytesChunkReceived += bytesReceived;
                    }

                    if (numBytesChunkReceived != 0) // Our previous chunk may have been the last one
                    {
                        file.Write(buffer, 0, numBytesChunkReceived);

                        toReceive -= numBytesChunkReceived;
                        byteRicevuti += numBytesChunkReceived;

                        Debug.Print("Da ricevere: " + toReceive + " bytes");
                        Debug.Print("Ricevuti: " + byteRicevuti + " bytes");

                    }
                    if (toReceive == 0) // We didn't write a full chunk: we're done
                        break;
                }
                Debug.Print("Totale ricevuti: " + byteRicevuti + " bytes");
                file.Close();

                if (byteRicevuti != originalDimFile)
                    return false;
                else
                    return true;
            }
            catch (Exception e)
            {
                file.Close();
                return false;
            }

        } 
        #endregion

        #region CreateFile
        private FileStream CreateFile(ClipboardFile clipboadFile)
        {
            FileStream file = null;
            try
            {
                if (!clipboadFile.Directory.Equals(""))
                {
                    if (!Directory.Exists(_path + clipboadFile.Directory))
                    {
                        DirectoryInfo directory = Directory.CreateDirectory(_path + clipboadFile.Directory);
                    }
                    // Store the file into the folder
                    file = new FileStream(_path + clipboadFile.Directory + @"\" + clipboadFile.Name, FileMode.Create, FileAccess.ReadWrite);
                }
                else // otherwise store the file into C:\temp\
                {
                    file = new FileStream(_path + clipboadFile.Name, FileMode.Create, FileAccess.ReadWrite);
                }
                file.Flush();
            }
            catch (Exception e)
            {
                file.Close();
                OnErrorReceive(new MyEventArgs(null));
            }

            return file;
        }
        #endregion

        #region DeserializeFromBytes
        private Object DeserializeFromBytes(byte[] buffer)
        {
            Object obj = null;
            try
            {
                MemoryStream ms = new MemoryStream(buffer);
                BinaryFormatter bf = new BinaryFormatter();
                obj = bf.Deserialize(ms);
                ms.Close();
            }
            catch (Exception e)
            {
                ErrorReceive(typeof(ReceiveClipboard), new MyEventArgs(e.StackTrace.ToString()));
            }

            return obj;
        }
        #endregion

        #region OnErrorReceive
        protected void OnErrorReceive(MyEventArgs e)
        {
            if (ErrorReceive != null)
                ErrorReceive(this, e);
        }
        #endregion

        #region OnFinishReceive
        // this event is raised when one file is finished to receive
        protected void OnFinishReceive(MyEventArgs e)
        {
            if (FinishReceive != null)
                FinishReceive(this, e);
        }
        #endregion
    }
}
