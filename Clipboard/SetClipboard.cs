﻿using ObjectSerializable;
using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Windows;
using System.Windows.Media.Imaging;

namespace ClipboardControl
{
    public class SetClipboard
    {
        #region SetText
        public static void SetText(ClipboardItem item)
        {
            Thread thread = new Thread(() =>
            {
                try
                {
                    ClipboardText text = (ClipboardText)item;
                    DataObject dataObjet = new DataObject();
                    for (int i = 0; i < text.Data.Count; i++)
                    {
                        string format = (string)text.Data[i++];
                        dataObjet.SetData(format, text.Data[i]);
                    }
                    Clipboard.SetDataObject(dataObjet, true);
                }
                catch (Exception)
                {
                    Debug.Print("Exception raising into Set Text");
                }
            });
            thread.SetApartmentState(ApartmentState.STA); //Set the thread to STA
            thread.Start();
            thread.Join();
        } 
        #endregion

        #region SetImage
        public static void SetImage(ClipboardItem item)
        {
            Thread thread = new Thread(() =>
            {
                try
                {
                    ClipboardImage clipboardImage = (ClipboardImage)item;

                    BitmapImage image = new BitmapImage();
                    image.BeginInit();
                    image.StreamSource = clipboardImage.Image;
                    image.EndInit();

                    DataObject dataObjet = new DataObject();
                    dataObjet.SetImage(image);

                    Clipboard.SetDataObject(dataObjet, true);
                }
                catch (Exception e)
                {
                    Debug.Print("Exception raising into Set Image");
                }
            });
            thread.SetApartmentState(ApartmentState.STA); //Set the thread to STA
            thread.Start();
            thread.Join();
        } 
        #endregion

        #region SetFile
        /// <summary>
        /// Set only name of file into _clipboard, not the real file
        /// </summary>
        /// <param name="item"></param>
        public static void SetFile(ClipboardItem item)
        {
            // lambda function
            Thread thread = new Thread(() =>
            {
                try
                {
                    ClipboardFileList clipboardFile = (ClipboardFileList)item;
                    DataObject dataObjet = new DataObject();

                    dataObjet.SetFileDropList(clipboardFile.ClipboardList);

                    Clipboard.SetDataObject(dataObjet, true);
                }
                catch (Exception)
                {
                    Debug.Print("Exception raising into Set File");
                }
            });
            thread.SetApartmentState(ApartmentState.STA); //Set the thread to STA
            thread.Start();
            thread.Join();
        } 
        #endregion

        #region SetAudio
        public static void SetAudio(ClipboardItem item)
        {
            Thread thread = new Thread(() =>
            {
                try
                {
                    ClipboardAudio clipboarAudio = (ClipboardAudio)item;

                    DataObject dataObjet = new DataObject();
                    dataObjet.SetAudio(clipboarAudio.Audio);

                    Clipboard.SetDataObject(dataObjet, true);
                }
                catch (Exception e)
                {
                    Debug.Print("Exception raising into Set Image");
                }
            });
            thread.SetApartmentState(ApartmentState.STA); //Set the thread to STA
            thread.Start();
            thread.Join();
        } 
        #endregion
    }
}