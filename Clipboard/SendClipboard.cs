﻿using Event;
using ObjectSerializable;
using System;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using System.Windows;

namespace ClipboardControl
{
    public delegate void SendEventHandler(object sender, MyEventArgs e);

    public class SendClipboard
    {        
        public event SendEventHandler ErrorSend;
        public event SendEventHandler FinishSend;
        public event SendEventHandler UpdateProgressBarSend;

        private Socket _clipboard;
        private const int BUFFER_SIZE = 1024 * 1024;
        private const string _path = @"C:\temp\";

        #region SendClipboard
        /// <summary>
        /// Detect how is contained into the _clipboard and delegate to sent to specific method
        /// </summary>
        /// <param name="clip">_clipboard</param>
        /// <param name="s">status</param>
        public SendClipboard(Socket clip)
        {
            _clipboard = clip;
        }
        #endregion

        #region StartSend
        public void StartSend()
        {
            try
            {
                #region Text
                if (Clipboard.ContainsText())
                {
                    ClipboardText text = GetClipboard.GetText();
                    if (text != null)
                        ClipboardText(text);
                }
                #endregion

                #region Image
                else if (Clipboard.ContainsImage())
                {
                    ClipboardImage image = GetClipboard.GetImage();
                    if(image != null)
                        ClipboardImage(image);
                }
                #endregion

                #region Audio
                else if (Clipboard.ContainsAudio())
                {
                    ClipboardAudio audio = GetClipboard.GetAudio();
                    if(audio != null)
                        ClipboardAudio(audio);
                }
                #endregion

                #region File
                else if (Clipboard.ContainsFileDropList())
                {
                    ClipboardFileList clipFileList = GetClipboard.GetFiles(_path);
                    if(clipFileList != null)
                        ClipboardFileList(clipFileList);
                }
                #endregion

            }
            catch (Exception e)
            {
                /* In case of exception thows event "ErrorSend" that is catch inside object
                 * that have invoked this method */
                OnErrorSend(new MyEventArgs(null));
            }
        } 
        #endregion

        #region ClipboardImage
        /// <summary>
        /// Convert the ClipboardImage object to array of bytes.
        /// </summary>
        /// <param name="image"></param>
        private void ClipboardImage(ClipboardImage image)
        {
            try
            {
                byte[] dataBytes = serializeToBytes(image);
                // I don't know the length of the object, then fist i send the lenght of the object because it isn't static.
                byte[] objLen = BitConverter.GetBytes(dataBytes.Length);
                StateClipboardObjectSender stateClipboard = new StateClipboardObjectSender();
                stateClipboard.type = "Immagine";
                // The byte that contains object
                stateClipboard.buffer = dataBytes;
                stateClipboard.toSend = dataBytes.Length;

                _clipboard.BeginSend(objLen, 0, objLen.Length, 0, new AsyncCallback(SendLengthClipboardCallback), stateClipboard);
            }
            catch (Exception e)
            {
                /* In case of exception thows event "ErrorSend" that is catch inside object
                 * that have invoked this method */
                OnErrorSend(new MyEventArgs(null));
            }

        }
        #endregion

        #region ClipboardAudio
        /// <summary>
        /// Convert the ClipboardAudio object to array of bytes.
        /// </summary>
        /// <param name="audio"></param>
        private void ClipboardAudio(ClipboardAudio audio)
        {
            try
            {
                byte[] dataBytes = serializeToBytes(audio);
                // I don't know the length of the object, then fist i send the lenght of the object because it isn't static.
                byte[] objLen = BitConverter.GetBytes(dataBytes.Length);
                StateClipboardObjectSender stateClipboard = new StateClipboardObjectSender();
                stateClipboard.type = "Audio";
                // The byte that contains object
                stateClipboard.buffer = dataBytes;
                // The amount of bytes to send
                stateClipboard.toSend = dataBytes.Length;

                _clipboard.BeginSend(objLen, 0, objLen.Length, 0, new AsyncCallback(SendLengthClipboardCallback), stateClipboard);
            }
            catch (Exception e)
            {
                /* In case of exception thows event "ErrorSend" that is catch inside object
                 * that have invoked this method */
                OnErrorSend(new MyEventArgs(null));
            }

        }
        #endregion

        #region ClipboarText
        /// <summary>
        /// Convert the ClipboardText object to array of bytes.
        /// </summary>
        /// <param name="text"></param>
        private void ClipboardText(ClipboardText text)
        {
            try
            {
                byte[] dataBytes = serializeToBytes(text);
                // I don't know the length of the object, then fist i send the lenght of the object because it isn't static.
                byte[] objLen = BitConverter.GetBytes(dataBytes.Length);
                StateClipboardObjectSender stateClipboard = new StateClipboardObjectSender();
                stateClipboard.type = "Testo";
                // The byte that contains object
                stateClipboard.buffer = dataBytes;
                // The amount bytes to send
                stateClipboard.toSend = dataBytes.Length;

                _clipboard.BeginSend(objLen, 0, objLen.Length, 0, new AsyncCallback(SendLengthClipboardCallback), stateClipboard);
            }
            catch (Exception e)
            {
                /* In case of exception thows event "ErrorSend" that is catch inside object
                 * that have invoked this method */
                OnErrorSend(new MyEventArgs(null));
            }

        }
        #endregion

        #region ClipboardFileList
        /// <summary>
        /// Convert the ClipboardFileList object to array of bytes.
        /// </summary>
        /// <param name="fileList"></param>
        private void ClipboardFileList(ClipboardFileList fileList)
        {
            try
            {
                byte[] dataBytes = serializeToBytes(fileList);
                // I don't know the length of the object, then fist i send the lenght of the object because it isn't static.
                byte[] objLen = BitConverter.GetBytes(dataBytes.Length);
                StateClipboardObjectSender stateClipboard = new StateClipboardObjectSender();
                stateClipboard.type = "File";
                // In this case is present a file
                stateClipboard.filePresent = true;
                stateClipboard.fileList = fileList.FileList;
                // The byte that contains object
                stateClipboard.buffer = dataBytes;
                // The amount bytes to send
                stateClipboard.toSend = dataBytes.Length;

                _clipboard.BeginSend(objLen, 0, objLen.Length, 0, new AsyncCallback(SendLengthClipboardCallback), stateClipboard);
            }
            catch (Exception e)
            {
                /* In case of exception thows event "ErrorSend" that is catch inside object
                 * that have invoked this method */
                OnErrorSend(new MyEventArgs(null));
            }

        }
        #endregion

        #region SendCLengthlipboardCallback
        /// <summary>
        /// Send the length of the object
        /// </summary>
        /// <param name="ar"></param>
        private void SendLengthClipboardCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                StateClipboardObjectSender stateClipboard = (StateClipboardObjectSender)ar.AsyncState;

                // Complete sending the data to the remote device.
                int bytesSent = _clipboard.EndSend(ar);
                Debug.Print("Lenght: " + bytesSent);

                _clipboard.BeginSend(stateClipboard.buffer, 0, stateClipboard.buffer.Length, 0, new AsyncCallback(SendObjectClipboardCallback), stateClipboard);

            }
            catch (Exception e)
            {
                /* In case of exception thows event "ErrorSend" that is catch inside object
                 * that have invoked this method */
                OnErrorSend(new MyEventArgs(null));
            }
        }
        #endregion

        #region SendObjectClipboardCallback
        /// <summary>
        /// Send the real object into the byte[]
        /// </summary>
        /// <param name="ar"></param>
        private async void SendObjectClipboardCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                StateClipboardObjectSender stateClipboard = (StateClipboardObjectSender)ar.AsyncState;

                // Complete sending the Object
                int bytesSent = _clipboard.EndSend(ar);
                Debug.Print("obj: " + bytesSent);

                // if not present file, comunicate that the send is done, otherwise i send single file
                // In particular: before send the leght of the file and after send the file in chunk (if too big)
                if (stateClipboard.filePresent == true)
                {
                    bool success = await SendAllFile(stateClipboard);

                    if(success)
                        OnFinishSend(new MyEventArgs("File inviato/i"));
                    else if (!success)
                        OnErrorSend(new MyEventArgs("Errore invio file"));
                }
                else
                    OnFinishSend(new MyEventArgs(stateClipboard.type+ " inviato"));
            }
            catch (Exception e)
            {
                /* In case of exception thows event "ErrorSend" that is catch inside object
                 * that have invoked this method */
                OnErrorSend(new MyEventArgs(null));
            }
        }
        #endregion

        #region
        private async Task<bool> SendAllFile(StateClipboardObjectSender stateClipboard)
        {
            bool temp = await Task.Run(() =>
            {
                long totalFileLen = 0;
                foreach (ClipboardFile cf in stateClipboard.fileList)
                {
                    totalFileLen += cf.FileLen;
                }
                long totalByteInviated = 0;

                var progress = new Progress<long>(byteSent =>
                {
                    double value = (byteSent * 100.0) / totalFileLen;
                    OnUpdateProgressBar(new MyEventArgs(value));
                });

                foreach (ClipboardFile cf in stateClipboard.fileList)
                {
                    bool success = SendSingleFile(cf.FullName, ref totalByteInviated, progress);

                    if (!success)
                        return false;
                }
                return true;
            });
            return temp;
        }
        #endregion

        #region SendSingleFile
        /// <summary>
        /// Send file stored into list of ClipboardFileList
        /// </summary>
        /// <param name="filePath"> The full _path to retrieve the file</param>
        private bool SendSingleFile(String filePath,ref long totalByteInviated, IProgress<long> progress)
        {
            FileStream file = null;
            try
            {
                file = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                long fileSize = file.Length;

                /* don't send the dimension of the file because it is storend inside ClipboadFile object */
                Debug.Print("Dimesione file: " + fileSize + " bytes" + " of :" + filePath);

                long bytemandati = 0;
                long originalDimFile = fileSize;
                /* loop until it isn't sent all file */
                byte[] buffer = new byte[BUFFER_SIZE];
                while(true)
                {
                    int numBytesRead = 0;
                    // There are various different ways of structuring this bit of code.
                    // Fundamentally we're trying to keep reading in to our chunk until
                    // either we reach the end of the stream, or we've read everything we need.
                    while (numBytesRead < buffer.Length)
                    {
                        int bytesRead = file.Read(buffer, 0, buffer.Length - numBytesRead);
                        if (bytesRead == 0)
                        {
                            break;
                        }
                        numBytesRead += bytesRead;
                    }
                    if (numBytesRead != 0) // Our previous chunk may have been the last one
                    {
                        int numBytesSend = 0;
                        while (numBytesSend < numBytesRead){
                            numBytesSend = _clipboard.Send(buffer, numBytesSend, numBytesRead - numBytesSend, SocketFlags.None);
                        }
                        fileSize -= numBytesRead;
                        bytemandati += numBytesSend;
                        totalByteInviated += numBytesSend;

                        Debug.Print("Da mandare: " + fileSize + " bytes");
                        Debug.Print("Mandati: " + bytemandati + " bytes");

                        progress.Report(totalByteInviated);
                    }

                    if (numBytesRead != buffer.Length) // We didn't read a full chunk: we're done
                    {
                        break;
                    }
                }
                Debug.Print("Totale inviati: " + bytemandati + " bytes");
                file.Close();

                if (bytemandati != originalDimFile)
                    return false;
                else
                    return true;
            }
            catch (Exception e)
            {
                /* In case of exception thows event "ErrorSend" that is catch inside object
                 * that have invoked this method */
                file.Close();
                return false;
            }

        }
        #endregion

        #region serializeToBytes
        private byte[] serializeToBytes(Object obj)
        {
            byte[] objj = null;

            try
            {
                MemoryStream memoryStream = new MemoryStream();
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(memoryStream, obj);

                objj = memoryStream.GetBuffer();
                memoryStream.Close();
            }
            catch (Exception e)
            {
                /* In case of exception thows event "ErrorSend" that is catch inside object
                 * that have invoked this method */
                OnErrorSend(new MyEventArgs(null));
            }

            return objj;
        }
        #endregion

        #region ConvertStremToMemory
        private byte[] convertStremToMemory(Stream str)
        {
            byte[] buffer = new byte[16 * 1024];
            MemoryStream ms = new MemoryStream();
            for (int read = 0; (read = str.Read(buffer, 0, buffer.Length)) > 0; )
            {
                ms.Write(buffer, 0, read);
            }
            return ms.ToArray();
        }
        #endregion

        #region OnErrorSend
        protected  void OnErrorSend(MyEventArgs e)
        {
            if (ErrorSend != null)
                ErrorSend(this, e);
        }
        #endregion

        #region OnFinishSend
        protected void OnFinishSend(MyEventArgs e)
        {
            if (FinishSend != null)
                FinishSend(this, e);
        }
        #endregion

        #region OnUpdateProgressBar
        protected void OnUpdateProgressBar(MyEventArgs e)
        {
            if (UpdateProgressBarSend != null)
                UpdateProgressBarSend(this, e);
        }
        #endregion
    }
}
