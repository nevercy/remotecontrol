﻿using ObjectSerializable;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Media.Imaging;

namespace ClipboardControl
{
    class GetClipboard
    {
        #region GetText
        public static ClipboardText GetText()
        {
            ClipboardText text = null;

            Thread thread = new Thread(() =>
            {
                Debug.Print("Contains Text");
                ArrayList list = new ArrayList();
                IDataObject contentsClipboard = Clipboard.GetDataObject();
                String[] formats = contentsClipboard.GetFormats();
                for (int i = 0; i < formats.Length; i++)
                {
                    if (!formats[i].Contains("Text") || !formats[i].Contains("Unicode"))
                        continue;
                    Object item = contentsClipboard.GetData(formats[i]);
                    if (item != null )  
                    {
                        if (item.GetType().IsSerializable) //add isValidFormat
                        {
                            //Debug.Print("Format: {0}", formats[i]);
                            list.Add(formats[i]);
                            list.Add(item);
                            //Debug.Print("Add");
                        }
                    }
                }

                text = new ClipboardText("text", list);
            });
            thread.SetApartmentState(ApartmentState.STA); //Set the thread to STA
            thread.Start();
            thread.Join();

            return text;

        }
        #endregion

        #region GetAudio
        public static ClipboardAudio GetAudio()
        {
            ClipboardAudio audio = null;
            Thread thread = new Thread(() =>
            {
                Debug.Print("Contains audio");
                MemoryStream ms = (MemoryStream)Clipboard.GetAudioStream();
                //MemoryStream ms = new MemoryStream(convertStremToMemory(stream));
                audio = new ClipboardAudio("audio", ms);
            });
            thread.SetApartmentState(ApartmentState.STA); //Set the thread to STA
            thread.Start();
            thread.Join();

            return audio;
        }
        #endregion

        #region GetImage
        public static ClipboardImage GetImage()
        {
            ClipboardImage image = null;
            Thread thread = new Thread(() =>
            {
                Debug.Print("Contains Image");
                MemoryStream ms = new MemoryStream();
                BmpBitmapEncoder encoder = new BmpBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(Clipboard.GetImage()));
                encoder.Save(ms);
                ms.Seek(0, SeekOrigin.Begin);

                image = new ClipboardImage("image", ms);
            });
            thread.SetApartmentState(ApartmentState.STA); //Set the thread to STA
            thread.Start();
            thread.Join();

            return image;
        }
        #endregion

        #region GetFiles
        public static ClipboardFileList GetFiles(String _path)
        {
            ClipboardFileList clipFileList = null;
            Thread thread = new Thread(() =>
            {
                Debug.Print("Contains file");
                /* Element retrive by _clipboard */
                StringCollection fileList = null;

                fileList = Clipboard.GetFileDropList();
                /* Collection of the names of the file to be send */
                StringCollection fileToSend = new StringCollection();
                /* List of file to be send also if it is a single file */
                List<ClipboardFile> list = new List<ClipboardFile>();

                foreach (String item in fileList)
                {
                    // Retrive the info file
                    FileInfo fileInfo = new FileInfo(item);
                    FileAttributes attributesFile = fileInfo.Attributes;
                    /* Check if the file is a directory */
                    if ((attributesFile & FileAttributes.Directory) == FileAttributes.Directory)
                    {
                        /* Add list of file contents into the folder */
                        FillList(fileInfo.FullName, fileInfo.Name, list);
                    }
                    else
                    {
                        ClipboardFile file = new ClipboardFile(
                            fileInfo.Name,
                            fileInfo.Length,
                            "",  //root directory
                            fileInfo.FullName
                            );
                        /* Add single file */
                        list.Add(file);
                    }
                    fileToSend.Add(_path + fileInfo.Name);
                }
                clipFileList = new ClipboardFileList("file", list, fileToSend);

            });
            thread.SetApartmentState(ApartmentState.STA); //Set the thread to STA
            thread.Start();
            thread.Join();

            return clipFileList;
        } 
        #endregion

        #region FillList
        /// <summary>
        /// For each file store in the folder add file into the list.
        /// </summary>
        /// <param name="path"> Absolute paht of the folder</param>
        /// <param name="dirName"> Name of folder</param>
        /// <param name="listFile"> List to fill </param>
        private static void FillList(String path, String dirName, List<ClipboardFile> listFile)
        {
            try
            {
                // for each file store in the folder, save the directory where it is contained
                foreach (String f in Directory.GetFiles(path))
                {
                    FileInfo fileInfo = new FileInfo(f);
                    ClipboardFile file = new ClipboardFile(
                        fileInfo.Name,
                        fileInfo.Length,
                        path.Substring(path.IndexOf(dirName)),
                        fileInfo.FullName
                        );
                    listFile.Add(file);
                }

                // if in the current directory are present another directories
                foreach (String d in Directory.GetDirectories(path))
                {
                    FillList(d, dirName, listFile);
                }
            }
            catch (Exception e)
            {
                /* In case of exception thows event "ErrorSend" that is catch inside object
                 * that have invoked this method */
               // OnErrorSend(new MyEventArgs(e.StackTrace.ToString()));
            }
        }
        #endregion
    }
}
