﻿using System;
using System.Text;
using System.Windows;
using System.Net.Sockets;
using System.Windows.Controls;
using System.Windows.Input;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Net;


namespace ClientSocket
{
    public partial class Client : Window
    {

        public static Socket senderMouse;
        public static Socket senderKeyboard;
        public static Socket Sender_Receive_Clipboard;

        public static ControlMouseAndKey secondWindow;
        public EventHandler handlerClose;

        public Client()
        {
            InitializeComponent();

            Disconnect_Button.IsEnabled = false;
        }


        #region Connect
        private void Connect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string ip = "192.168.20.128";
                //IPAddress ipAddr = IPAddress.Parse(getIP.Text);
                IPAddress ipAddr = IPAddress.Parse(ip);

                /* Creates a network endpoint */
                IPEndPoint ipEndPointM = new IPEndPoint(ipAddr, 4510);
                IPEndPoint ipEndPointK = new IPEndPoint(ipAddr, 4511);
                IPEndPoint ipEndPointC = new IPEndPoint(ipAddr, 4512);

                /* Create one Socket object to setup Tcp connection */
                senderMouse = new Socket(
                    ipAddr.AddressFamily,// Specifies the addressing scheme 
                    SocketType.Stream,   // The type of socket  
                    ProtocolType.Tcp     // Specifies the protocols  
                    );
                senderKeyboard = new Socket(
                    ipAddr.AddressFamily,// Specifies the addressing scheme 
                    SocketType.Stream,   // The type of socket  
                    ProtocolType.Tcp     // Specifies the protocols  
                    );
                Sender_Receive_Clipboard = new Socket(
                    ipAddr.AddressFamily,// Specifies the addressing scheme 
                    SocketType.Stream,   // The type of socket  
                    ProtocolType.Tcp     // Specifies the protocols  
                    );

                /* Disable Nagle algorithm */
                senderMouse.NoDelay = true; 
                senderKeyboard.NoDelay = true;
                Sender_Receive_Clipboard.NoDelay = true; 

                /* Establishes a connection to a remote host */
                senderMouse.Connect(ipEndPointM);
                senderKeyboard.Connect(ipEndPointK);
                Sender_Receive_Clipboard.Connect(ipEndPointC);

                Disconnect_Button.IsEnabled = false;
                Connect_Button.IsEnabled = false;

                /* Open new window */
                secondWindow = new ControlMouseAndKey();
                secondWindow.Show();

                /* Add handler to detect when it's close */
                secondWindow.Closed += Disconnect_Click;

            }
            catch (Exception exc) { MessageBox.Show(exc.ToString()); }

        }
        #endregion


        #region disconnect
        private void Disconnect_Click(object sender, EventArgs e)
        {
            try
            {
                // Disables sends and receives on a Socket. 
                senderMouse.Shutdown(SocketShutdown.Both);
                senderKeyboard.Shutdown(SocketShutdown.Both);
                Sender_Receive_Clipboard.Shutdown(SocketShutdown.Both);

                //Closes the Socket connection and releases all resources 
                senderMouse.Close();
                senderKeyboard.Close();
                Sender_Receive_Clipboard.Close();

                Disconnect_Button.IsEnabled = false;
                Connect_Button.IsEnabled = true;

                secondWindow.Closed -= Disconnect_Click;

                MessageBox.Show("Disconnessione avvenuta correttamente");
            }
            catch (Exception exc) { MessageBox.Show(exc.ToString()); }
        }
        #endregion
       
    }

}
