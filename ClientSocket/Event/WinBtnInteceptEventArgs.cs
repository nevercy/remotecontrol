﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ClientSocket
{
    public delegate void WinBtnInteceptEventHandler(object sender, WinBtnInteceptEventArgs e);
    public class WinBtnInteceptEventArgs : System.EventArgs
    {
        ModifierKeys modifier;

        public ModifierKeys Modifier
        {
            get { return modifier; }
            set { modifier = value; }
        }
        
        Key key;

        public Key Key
        {
            get { return key; }
            set { key = value; }
        }

    }
}
