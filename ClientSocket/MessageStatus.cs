﻿using Event;
using ObjectSerializable;
using System;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Text;

namespace ClientSocket
{
    class MessageStatus
    {
        private Socket _message;

        public event BigClientEventHandler OkPassword;
        public event BigClientEventHandler WrongPassword;
        public event BigClientEventHandler MessageError;

        public MessageStatus(ref Socket m)
        {
            _message = m;
        }

        public void SendMessage(string message)
        {
            byte[] msg = Encoding.ASCII.GetBytes(message);
            _message.BeginSend(msg, 0, msg.Length, 0, new AsyncCallback(SendStatusCallback), _message);
        }

        #region SendStatusCallback
        private void SendStatusCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket client = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.
                int bytesSent = client.EndSend(ar);
                if (bytesSent < 0)
                    OnMessageError(new MyEventArgs("Connection closed by party"));

                //Debug.Print("Sent {0} bytes to server.", bytesSent);

                // Signal that all bytes have been sent.
                //sendDone.Set();
            }
            catch (Exception)
            {
                OnMessageError(new MyEventArgs("Error send _message to the server"));
            }
        }
        #endregion

        public void ReceiveMessage()
        {
            StateStatusObject stateStatus = new StateStatusObject();
            stateStatus.workSocket = _message;
            stateStatus.toReceive = stateStatus.buffer.Length;

            _message.BeginReceive(
                        stateStatus.buffer,          // An array of type Byt for received data 
                        0,                          // The zero-based position in the buffer  
                        StateStatusObject.BufferSize,// The number of bytes to receive 
                        SocketFlags.None,           // Specifies send and receive behaviors 
                        new AsyncCallback(ReceiveStatusCallback),//An AsyncCallback delegate 
                        stateStatus            // Specifies infomation for receive operation 
                        );
        }

        #region ReceiveStatusCallback
        private void ReceiveStatusCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the handler socket
                // from the asynchronous state object.
                StateStatusObject stateStatusObject = (StateStatusObject)ar.AsyncState;
                Socket handler = stateStatusObject.workSocket;

                // Read data from the client socket. 
                int bytesRead = handler.EndReceive(ar);

                if (bytesRead > 0)
                {
                    //stateStatusObject.toReceive -= bytesRead;
                    stateStatusObject.toReceive -= 256;
                    Debug.Print("STATUS: Recived {0} bytes", bytesRead);

                    if (stateStatusObject.toReceive == 0)
                    {
                        //Debug.Print("STATUS: Da ricevere {0} bytes", stateStatusObject.toReceive);
                        StateStatusObject newStateStatusObject = new StateStatusObject();
                        newStateStatusObject.workSocket = handler;
                        newStateStatusObject.toReceive = StateStatusObject.BufferSize;

                        if (stateStatusObject.ms != null)
                            EventStatusReceived(stateStatusObject.ms.GetBuffer(), 256);
                        else
                            EventStatusReceived(stateStatusObject.buffer, bytesRead);
                    }
                    else
                    {
                        //Debug.Print("STATUS: Da ricevere {0} bytes", stateStatusObject.toReceive);
                        stateStatusObject.ms = new MemoryStream();
                        // Transfer the imcomplete object into the steam.
                        stateStatusObject.ms.Write(stateStatusObject.buffer, 0, bytesRead);
                        // Create a new buffer ready for new incoming bytes.
                        stateStatusObject.buffer = new byte[StateStatusObject.BufferSize];
                        handler.BeginReceive(
                            stateStatusObject.buffer,            // An array of type Byt for received data 
                            0,                                  // The zero-based position in the buffer  
                            StateStatusObject.BufferSize,        // The number of bytes to receive 
                            SocketFlags.None,                   // Specifies send and receive behaviors 
                            new AsyncCallback(ReceiveStatusCallback), //An AsyncCallback delegate 
                            stateStatusObject                    // Specifies infomation for receive operation 
                            );
                    }
                }
                else
                    OnMessageError(new MyEventArgs("Connection closed by party"));
            }
            catch (Exception e)
            {
                OnMessageError(new MyEventArgs("Error receive _message from Server"));
            }
        }
        #endregion

        #region EventStatusReceived
        private void EventStatusReceived(byte[] data, int dataRead)
        {
            String content = String.Empty;
            StringBuilder sb = new StringBuilder();
            sb.Append(Encoding.ASCII.GetString(data, 0, dataRead));

            //Debug.Print("STATUS: Event _message received");
            content = sb.ToString();
            Debug.Print("STATUS: content" + content);

            MyEventArgsPort eventArgs = null;
            if (content.Contains("OkPassword"))
            {
                eventArgs = new MyEventArgsPort("OkPassword");
                string[] words = content.Split(' ');
                eventArgs.PortMouse = Int32.Parse(words[1]);
                eventArgs.PortKeyboard = Int32.Parse(words[2]);
                eventArgs.PortClipboard = Int32.Parse(words[3]);

                content = "OkPassword";
            }

            switch (content)
            {
                case "OkPassword":
                    OnOkPassword(eventArgs);
                    break;
                case "WrongPassword":
                    OnWrongPassword(new MyEventArgs("Wrong password inserted"));
                    break;
                case "Closing":
                    OnMessageError(new MyEventArgs("Server chiuso"));
                    break;
            }
        }
        #endregion

        #region OnOkPassword
        protected virtual void OnOkPassword(MyEventArgs e)
        {
            if (OkPassword != null)
                OkPassword(this, e);
        }
        #endregion

        #region OnMessageError
        protected virtual void OnMessageError(MyEventArgs e)
        {
            if (MessageError != null)
                MessageError(this, e);
        }
        #endregion

        #region OnMessageError
        protected virtual void OnWrongPassword(MyEventArgs e)
        {
            if (WrongPassword != null)
                WrongPassword(this, e);
        }
        #endregion
    }
}
