﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ClientSocket
{
    sealed class KeyboardHook
    {
        public static event WinBtnInteceptEventHandler WinBtnIntercepted_DOWN;
        public static event WinBtnInteceptEventHandler WinBtnIntercepted_UP;

        #region Private Members

        private delegate IntPtr HookHandlerDelegate(int nCode, IntPtr wParam, ref KBHookStruct lParam);
        private static HookHandlerDelegate callbackPtr;
        private static IntPtr hookPtr = IntPtr.Zero;
        private const int LowLevelKeyboardHook = 13;

        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook, HookHandlerDelegate callbackPtr, IntPtr hInstance, uint dwThreadId);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, ref KBHookStruct lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        public static extern short GetKeyState(int keyCode);
        
        //Keyboard API constants
        private const int WH_KEYBOARD_LL = 13;
        private const int WM_KEYUP = 0x0101;
        private const int WM_SYSKEYUP = 0x0105;
        private const int WM_KEYDOWN = 0x0100;
        private const int WM_SYSKEYDOWN = 0x0104;

        //Modifier key constants
        private const int VK_SHIFT = 0x10;
        private const int VK_CONTROL = 0x11;
        private const int VK_MENU = 0x12;
        private const int VK_CAPITAL = 0x14;
        private const int VK_ESCAPE = 0x1B;
        #endregion

        #region PrivateStruct
        [StructLayout(LayoutKind.Sequential)]
        private struct KBHookStruct
        {
            public int vkCode;
            public int scanCode;
            public int flags;
            public int time;
            public int dwExtraInfo;
        }
        #endregion

        #region CalledAtTheBegin
        public static void DisableSystemKeys()
        {
            if (callbackPtr == null)
            {
                callbackPtr = new HookHandlerDelegate(KeyboardHookHandler);
            }

            if (hookPtr == IntPtr.Zero)
            {
                // Note: This does not work in the VS host environment.  To run in debug mode:
                // Project -> Properties -> Debug -> Uncheck "Enable the Visual Studio hosting process"
                IntPtr hInstance = Marshal.GetHINSTANCE(Application.Current.GetType().Module);
                hookPtr = SetWindowsHookEx(LowLevelKeyboardHook, callbackPtr, hInstance, 0);
            }
        }
        #endregion

        #region CalledAtTheEnd
        public static void EnableSystemKeys()
        {
            if (hookPtr != IntPtr.Zero)
            {
                UnhookWindowsHookEx(hookPtr);
                hookPtr = IntPtr.Zero;
            }
        }
        #endregion

        #region IntPtr
        //Questa sarebbe la hook procedure che poi viene effettivamente chiamata. Ed ha una signature particolare.
        private static IntPtr KeyboardHookHandler(int nCode, IntPtr wParam, ref KBHookStruct lParam)
        {
            //Nelle documentazioni c'è scritto che se nCode è < 0 allora bisogna passare lo hook al CallNextHookEx
            //If nCode==0 The wParam and lParam parameters contain information about a keyboard message.
            //https://msdn.microsoft.com/en-us/library/ms644985(VS.85).aspx
            if (nCode == 0)
            {
                if ((wParam == (IntPtr)WM_KEYDOWN || wParam == (IntPtr)WM_SYSKEYDOWN) && (IsWindowOpen<Window>("WideClient")))
                {
                    #region Keys intercepted Blocked and Sended Only to The Server
                    if ((lParam.vkCode==0x2A)) {
                        //VK_PRINT
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = KeyInterop.KeyFromVirtualKey(lParam.vkCode);
                        WinBtnIntercepted_DOWN(null, e);
                        return new IntPtr(1);
                    }
                    if ((lParam.vkCode == 0x2C)) {
                        //PRINT SCREEN
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = KeyInterop.KeyFromVirtualKey(lParam.vkCode);
                        WinBtnIntercepted_DOWN(null, e);
                        return new IntPtr(1);
                    }
                    if ((lParam.vkCode == 0x5B) && (lParam.flags == 0x01))
                    {   // LEFT WIN KEY
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.LWin;
                        WinBtnIntercepted_DOWN(null, e);
                        return new IntPtr(1);
                    }
                    if ((lParam.vkCode == 0x73) && (lParam.flags == 0x20))
                    {   //ALT + F4 (That cause the command to close the application opened in that moment)
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.LeftAlt;
                        WinBtnIntercepted_DOWN(null, e);
                        e.Key = Key.F4;
                        WinBtnIntercepted_DOWN(null, e);
                        return new IntPtr(1);
                    }
                    if (((GetKeyState(VK_CONTROL) & 0x8000) != 0) &&
                        (lParam.vkCode == VK_ESCAPE))
                    {   //Ctrl + Esc (that cause the simulation of win button)
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.LeftCtrl;
                        WinBtnIntercepted_DOWN(null, e);
                        e.Key = Key.Escape;
                        WinBtnIntercepted_DOWN(null, e);
                        return new IntPtr(1);
                    }
                    if (((GetKeyState(VK_CONTROL) & 0x8000) != 0) &&
                        ((GetKeyState(VK_MENU) & 0x8000) != 0) &&
                        (lParam.vkCode == 0x09))
                    {
                        //CONTROL + ALT + TAB (That cause the possibility to change among the opened applications)
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.LeftCtrl;
                        WinBtnIntercepted_DOWN(null, e);
                        e.Key = Key.LeftAlt;
                        WinBtnIntercepted_DOWN(null, e);
                        e.Key = Key.Tab;
                        WinBtnIntercepted_DOWN(null, e);
                        return new IntPtr(1);

                    }
                    if ((((GetKeyState(0x5B) & 0x8000) != 0) || ((GetKeyState(0x5C) & 0x8000) != 0)) &&
                        (lParam.vkCode == 0x09))
                    {
                        //( L-WIN or R-WIN ) + TAB (That cause the possibility to change among the opened applications)
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.LWin;
                        WinBtnIntercepted_DOWN(null, e);
                        e.Key = Key.Tab;
                        WinBtnIntercepted_DOWN(null, e);
                        return new IntPtr(1);
                    }
                    if ((((GetKeyState(0x5B) & 0x8000) != 0) || ((GetKeyState(0x5C) & 0x8000) != 0)) &&
                        (lParam.vkCode == 0x54))
                    {
                        //( L-WIN or R-WIN ) + T (That cause the possibility to exit from BigClient)
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.LWin;
                        WinBtnIntercepted_DOWN(null, e);
                        e.Key = Key.T;
                        WinBtnIntercepted_DOWN(null, e);
                        return new IntPtr(1);
                    }
                    if ((lParam.vkCode>=0xA6)&& (lParam.vkCode<= 0xB7))
                    {
                        //BrowserButtons (That cause something could get out from the application client side like the opening of IE)
                        //VolumeButtons
                        //MediaButtons
                        //MailButtons
                        //LaunchAppButtons
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = KeyInterop.KeyFromVirtualKey(lParam.vkCode);
                        WinBtnIntercepted_DOWN(null, e);
                        return new IntPtr(1);
                    }
                    if (((GetKeyState(VK_CONTROL) & 0x8000) != 0) &&
                        ((GetKeyState(VK_SHIFT) & 0x8000) != 0) &&
                        (lParam.vkCode == 0x1B))
                    {
                        //CONTROL + SHIFT + ESC (That cause the opening of Task Manager)
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.LeftCtrl;
                        WinBtnIntercepted_DOWN(null, e);
                        e.Key = Key.LeftShift;
                        WinBtnIntercepted_DOWN(null, e);
                        e.Key = Key.Escape;
                        WinBtnIntercepted_DOWN(null, e);
                        return new IntPtr(1);

                    }
                    //To Undestand the value of the flags , read this page 
                    //https://msdn.microsoft.com/en-us/library/ms644967(v=vs.85).aspx
                    if ((lParam.vkCode == 0x5C) && (lParam.flags == 0x01))
                    {   // RIGHT WIN 
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.RWin;
                        WinBtnIntercepted_DOWN(null, e);
                        return new IntPtr(1);
                    }
                    if ((lParam.vkCode == 0x09) && (lParam.flags == 0x20))
                    { // ALT + TAB (That cause the possibility to change among the opened applications)
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.LeftAlt;
                        WinBtnIntercepted_DOWN(null, e);
                        e.Key = Key.Tab;
                        WinBtnIntercepted_DOWN(null, e);
                        return new IntPtr(1);
                    }
                    if ((lParam.vkCode == 0x1B) && (lParam.flags == 0x20))
                    { // ALT + ESC (That cause the possibility to change among the opened applications)
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.LeftAlt;
                        WinBtnIntercepted_DOWN(null, e);
                        e.Key = Key.Escape;
                        WinBtnIntercepted_DOWN(null, e);
                        return new IntPtr(1);
                    }
                    if ((lParam.vkCode == 0x20) && (lParam.flags == 0x20))
                    { // ALT + SPACE (That show the speed menu fot the opened window)
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.LeftAlt;
                        WinBtnIntercepted_DOWN(null, e);
                        e.Key = Key.Space;
                        WinBtnIntercepted_DOWN(null, e);
                        return new IntPtr(1); //Ossia Quando delle chiavi non sono ammesse
                    }
                    if (( (((GetKeyState(0x5B) & 0x8000) != 0) || ((GetKeyState(0x5C) & 0x8000) != 0)) && ((lParam.vkCode >= 0x30) || (lParam.vkCode <= 0x039)) && (lParam.flags == 0x20)))
                    { // LWIN or RWIN ,un NUM tra 0 e 9 , ALT (Causa apertura menu a tendina sulle singole app sulla taskbar)
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.LeftAlt;
                        WinBtnIntercepted_DOWN(null, e);
                        e.Key = Key.LWin;
                        WinBtnIntercepted_DOWN(null, e);
                        e.Key = KeyInterop.KeyFromVirtualKey(lParam.vkCode);
                        WinBtnIntercepted_DOWN(null, e);
                        return new IntPtr(1); //Ossia Quando delle chiavi non sono ammesse
                    }
                    #endregion
                }

                if ((wParam == (IntPtr)WM_KEYUP || wParam == (IntPtr)WM_SYSKEYUP) && (IsWindowOpen<Window>("WideClient")))
                {
                    #region Keys intercepted Blocked and Sended Only to The Server
                    if ((lParam.vkCode == 0x2A))
                    {
                        //VK_PRINT
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = KeyInterop.KeyFromVirtualKey(lParam.vkCode); ;
                        WinBtnIntercepted_UP(null, e);
                        return new IntPtr(1);
                    }
                    if ((lParam.vkCode == 0x2C))
                    {
                        //PRINT SCREEN
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = KeyInterop.KeyFromVirtualKey(lParam.vkCode);
                        WinBtnIntercepted_UP(null, e);
                        return new IntPtr(1);
                    }
                    if ((lParam.vkCode == 0x5B) && (lParam.flags == 0x01))
                    {   // LEFT WIN KEY
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.LWin;
                        WinBtnIntercepted_UP(null, e);
                        return new IntPtr(1);
                    }
                    if ((lParam.vkCode == 0x73) && (lParam.flags == 0x20))
                    {   //ALT + F4 (That cause the command to close the application opened in that moment)
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.LeftAlt;
                        WinBtnIntercepted_UP(null, e);
                        e.Key = Key.F4;
                        WinBtnIntercepted_UP(null, e);
                        return new IntPtr(1);
                    }
                    if (((GetKeyState(VK_CONTROL) & 0x8000) == 0) &&
                        (lParam.vkCode == VK_ESCAPE))
                    {   //Ctrl + Esc (that cause the simulation of win button)
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.LeftCtrl;
                        WinBtnIntercepted_UP(null, e);
                        e.Key = Key.Escape;
                        WinBtnIntercepted_UP(null, e);
                        return new IntPtr(1);
                    }
                    if (((GetKeyState(VK_CONTROL) & 0x8000) == 0) &&
                        ((GetKeyState(VK_MENU) & 0x8000) == 0) &&
                        (lParam.vkCode == 0x09))
                    {
                        //CONTROL + ALT + TAB (That cause the possibility to change among the opened applications)
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.LeftCtrl;
                        WinBtnIntercepted_UP(null, e);
                        e.Key = Key.LeftAlt;
                        WinBtnIntercepted_UP(null, e);
                        e.Key = Key.Tab;
                        WinBtnIntercepted_UP(null, e);
                        return new IntPtr(1);

                    }
                    if ((((GetKeyState(0x5B) & 0x8000) == 0) || ((GetKeyState(0x5C) & 0x8000) == 0)) &&
                        (lParam.vkCode == 0x09))
                    {
                        //( L-WIN or R-WIN ) + TAB (That cause the possibility to change among the opened applications)
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.LWin;
                        WinBtnIntercepted_UP(null, e);
                        e.Key = Key.Tab;
                        WinBtnIntercepted_UP(null, e);
                        return new IntPtr(1);
                    }
                    if ((((GetKeyState(0x5B) & 0x8000) == 0) || ((GetKeyState(0x5C) & 0x8000) == 0)) &&
                        (lParam.vkCode == 0x54))
                    {
                        //( L-WIN or R-WIN ) + T (That cause the possibility to exit from BigClient)
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.LWin;
                        WinBtnIntercepted_UP(null, e);
                        e.Key = Key.T;
                        WinBtnIntercepted_UP(null, e);
                        return new IntPtr(1);
                    }
                    if ((lParam.vkCode >= 0xA6) && (lParam.vkCode <= 0xB7))
                    {
                        //BrowserButtons (That cause something could get out from the application client side like the opening of IE)
                        //VolumeButtons
                        //MediaButtons
                        //MailButtons
                        //LaunchAppButtons
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = KeyInterop.KeyFromVirtualKey(lParam.vkCode);
                        WinBtnIntercepted_UP(null, e);
                        return new IntPtr(1);
                    }
                    if (((GetKeyState(VK_CONTROL) & 0x8000) == 0) &&
                        ((GetKeyState(VK_SHIFT) & 0x8000) == 0) &&
                        (lParam.vkCode == 0x1B))
                    {
                        //CONTROL + SHIFT + ESC (That cause the opening of Task Manager)
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.LeftCtrl;
                        WinBtnIntercepted_UP(null, e);
                        e.Key = Key.LeftShift;
                        WinBtnIntercepted_UP(null, e);
                        e.Key = Key.Escape;
                        WinBtnIntercepted_UP(null, e);
                        return new IntPtr(1);

                    }
                    //To Undestand the value of the flags , read this page 
                    //https://msdn.microsoft.com/en-us/library/ms644967(v=vs.85).aspx
                    if ((lParam.vkCode == 0x5C) && (lParam.flags == 0x01))
                    {   // RIGHT WIN 
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.RWin;
                        WinBtnIntercepted_UP(null, e);
                        return new IntPtr(1);
                    }
                    if ((lParam.vkCode == 0x09) && (lParam.flags == 0x20))
                    { // ALT + TAB (That cause the possibility to change among the opened applications)
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.LeftAlt;
                        WinBtnIntercepted_UP(null, e);
                        e.Key = Key.Tab;
                        WinBtnIntercepted_UP(null, e);
                        return new IntPtr(1);
                    }
                    if ((lParam.vkCode == 0x1B) && (lParam.flags == 0x20))
                    { // ALT + ESC (That cause the possibility to change among the opened applications)
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.LeftAlt;
                        WinBtnIntercepted_UP(null, e);
                        e.Key = Key.Escape;
                        WinBtnIntercepted_UP(null, e);
                        return new IntPtr(1);
                    }
                    if ((lParam.vkCode == 0x20) && (lParam.flags == 0x20))
                    { // ALT + SPACE (That show the speed menu fot the opened window)
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.LeftAlt;
                        WinBtnIntercepted_UP(null, e);
                        e.Key = Key.Space;
                        WinBtnIntercepted_UP(null, e);
                        return new IntPtr(1); //Ossia Quando delle chiavi non sono ammesse
                    }
                    if (((((GetKeyState(0x5B) & 0x8000) == 0) || ((GetKeyState(0x5C) & 0x8000) == 0)) && ((lParam.vkCode >= 0x30) || (lParam.vkCode <= 0x039)) && (lParam.flags == 0x20)))
                    { // LWIN or RWIN ,un NUM tra 0 e 9 , ALT (Causa apertura menu a tendina sulle singole app sulla taskbar)
                        WinBtnInteceptEventArgs e = new WinBtnInteceptEventArgs();
                        e.Key = Key.LeftAlt;
                        WinBtnIntercepted_UP(null, e);
                        e.Key = Key.LWin;
                        WinBtnIntercepted_UP(null, e);
                        e.Key = KeyInterop.KeyFromVirtualKey(lParam.vkCode);
                        WinBtnIntercepted_UP(null, e);
                        return new IntPtr(1); //Ossia Quando delle chiavi non sono ammesse
                    }
                    #endregion
                }
            }

            return CallNextHookEx(hookPtr, nCode, wParam, ref lParam); //Quando le vuole passare a windows perchè sono ammesse
        }
        #endregion

        #region Utilities
        //This function checks if the Window is Opened or Not Opened
        public static bool IsWindowOpen<T>(string name = "") where T : Window
        {
            return string.IsNullOrEmpty(name)
               ? Application.Current.Windows.OfType<T>().Any()
               : Application.Current.Windows.OfType<T>().Any(w => w.Name.Equals(name));
        }
        #endregion

    }
}

