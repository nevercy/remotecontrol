﻿using System;
using System.Windows;
using System.Net.Sockets;
using System.Net;
using System.IO;
using MahApps.Metro.Controls;
using System.Collections.Generic;
using System.Threading.Tasks;
using IconTray;
using System.Windows.Controls.Primitives;
using Baloon;
using Event;
using System.Text.RegularExpressions;
using System.Net.NetworkInformation;
using System.Diagnostics;
using System.Linq;

namespace ClientSocket
{
    public partial class SmallClient : MetroWindow
    {

        private Dictionary<int, BigClient> _clientsControl;
        private int _clients = 0;

        private int _portMessage;
        private int _portMouse;
        private int _portKeyboard;
        private int _portClipboard;

        private IPAddress ipAddr;
        private Socket status;

        private BaloonText balloon;

        private MessageStatus _messageStatus;
        private BigClient secondWindow;

        #region Init
        public SmallClient()
        {
            InitializeComponent();

            _clientsControl = new Dictionary<int, BigClient>();

            /* This is the folder where the client store the incoming file */
            if (!Directory.Exists(@"C:\temp\"))
                Directory.CreateDirectory(@"C:\temp\");

            ShowMessageCommand.ShowWindowEvent += ManageIconTray;

            balloon = new BaloonText();
            balloon.ChangeMessage("Welcome User");
            MyNotifyIcon.ShowCustomBalloon(balloon, PopupAnimation.Slide, 4000);
        }
        #endregion

        #region ChangeVisibility
        void ChangeVisibility()
        {
            if (Visibility == Visibility.Hidden || WindowState == WindowState.Minimized)
            {
                Show();
                WindowState = WindowState.Normal;
            }
        }
        #endregion

        #region ManageIconTray
        //This code is used to handle the visibility of the window
        //The handle of the event is activated and registered at the startup of the Server
        //How you can see the visibility is changed across the method Hide of the window
        //The event is sended to the .NET by clicking the context Menu on the icon tray
        void ManageIconTray(object s, EventArgs e)
        {
            IconEventArgs iea = (IconEventArgs)e;
            if (iea.Type == "reopen")
                ChangeVisibility();
            else
            {
                Window_Closed(this, e);
                Close();
            }
        }
        #endregion

        #region Connect
        private async void Connect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                secondWindow = null;
                if(Checkip())
                {
                    if (CheckPort())
                    {
                        if (CheckPassword())
                        {
                            IPEndPoint ipEndPointS = new IPEndPoint(ipAddr, _portMessage);  /* Status-comunication endpoint */

                            status = new Socket(
                                ipEndPointS.Address.AddressFamily,// Specifies the addressing scheme 
                                SocketType.Stream,   // The type of socket  
                                ProtocolType.Tcp     // Specifies the protocols  
                                );

                            /* Disable Nagle-Alghorithm */
                            status.NoDelay = true;

                            Connect_Button.IsEnabled = false;

                            //Thread t = new Thread( );
                            bool succes = await TryToConnect(ipEndPointS, status);

                            if (succes)
                            {

                                _messageStatus = new MessageStatus(ref status);
                                _messageStatus.OkPassword += OkPassword;
                                _messageStatus.WrongPassword += WrongPassword;
                                _messageStatus.ReceiveMessage();
                                _messageStatus.SendMessage(getPassword.Password);
                            }
                            else
                            {
                                errore.Content = "Errore Connessione";
                            }
                        }
                    }                          
                }
                Connect_Button.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("An exception just occurred: " + ex.StackTrace, "Eccezione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        #endregion

        #region Checkip
        private bool Checkip()
        {
            if (IPAddress.TryParse(getIP.Text, out ipAddr))
            {
                return true;
            }
            else
            {
                errore.Content = "Insert a valid ip address";
                return false;
            }
        }
        #endregion

        #region CheckPort
        private bool CheckPort()
        {
            try
            {
                if (!portToConnect.Text.Equals(""))
                {
                    bool isNumber = IsNumberAllowed(portToConnect.Text);

                    if (!isNumber)
                    {
                        errore.Content = "Insert a valid port numer";
                        return false;
                    }
                    else
                    {
                        int selectedPort = Int32.Parse(portToConnect.Text.ToString());

                        if (selectedPort < 1024 || selectedPort > 65535)
                        {
                            errore.Content = "Insert a port between 1024 and 65535";
                            return false;
                        }
                        else
                        {
                            // per le altre porte vedere il metodo checkpassword
                            _portMessage = selectedPort;
                            return true;
                        }
                    }
                }
                else
                {
                    errore.Content = "Inserire Porta";
                    return false;
                }
            }
            catch (Exception)
            {
                errore.Content = "Insert a port between 1025 and 65535";
                return false;
            }
        } 
        #endregion

        #region IsNumberAllowed
        private static bool IsNumberAllowed(string text)
        {
            Regex regex = new Regex(@"^[0-9]+$"); //regex that matches disallowed text
            return regex.IsMatch(text);
        }
        #endregion

        #region CheckPassword
        private bool CheckPassword()
        {
            if (!getPassword.Password.Equals(""))
            {
                return true;
            }
            else
            {
                errore.Content = "Inserire password";
                return false;
            }
        }
        #endregion

        #region TryToConnect
        // metodo asincrono
        private async Task<bool> TryToConnect(IPEndPoint ipEndPointS, Socket status)
        {

            try {
                await Task.Run(() =>
                {
                    status.Connect(ipEndPointS);
                });
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        #endregion

        #region OkPassword
        private void OkPassword(object sender, MyEventArgs e)
        {
            Dispatcher.BeginInvoke( new Action(() =>
            {
               try
               {
                    errore.Content = "";
                    _portMouse = (e as MyEventArgsPort).PortMouse;
                    _portKeyboard = (e as MyEventArgsPort).PortKeyboard;
                    _portClipboard = (e as MyEventArgsPort).PortClipboard;

                    /* Creates a network endpoint */
                    IPEndPoint ipEndPointM = new IPEndPoint(ipAddr, _portMouse);  /* Mouse endpoint */
                    IPEndPoint ipEndPointK = new IPEndPoint(ipAddr, _portKeyboard);  /* Keyboard endpoint */
                    IPEndPoint ipEndPointC = new IPEndPoint(ipAddr, _portClipboard);  /* Clipboard endpoint */


                    /* Create one Socket object to setup Tcp connection */
                    Socket senderMouse = new Socket(
                        ipEndPointM.Address.AddressFamily,// Specifies the addressing scheme 
                        SocketType.Stream,   // The type of socket  
                        ProtocolType.Tcp     // Specifies the protocols  
                        );
                    Socket senderKeyboard = new Socket(
                        ipEndPointK.Address.AddressFamily,// Specifies the addressing scheme 
                        SocketType.Stream,   // The type of socket  
                        ProtocolType.Tcp     // Specifies the protocols  
                        );
                    Socket Sender_Receive_Clipboard = new Socket(
                        ipEndPointC.Address.AddressFamily,// Specifies the addressing scheme 
                        SocketType.Stream,   // The type of socket  
                        ProtocolType.Tcp     // Specifies the protocols  
                        );

                    /* Disable Nagle algorithm */
                    senderMouse.NoDelay = true;
                    senderKeyboard.NoDelay = true;
                    Sender_Receive_Clipboard.NoDelay = true;

                    senderMouse.Connect(ipEndPointM);
                    senderKeyboard.Connect(ipEndPointK);
                    Sender_Receive_Clipboard.Connect(ipEndPointC);

                    secondWindow = new BigClient(senderMouse, senderKeyboard, Sender_Receive_Clipboard, status);

                    CloseOtherClient();
                    for (int i = 0; true; i++)
                    {
                        if (!_clientsControl.ContainsKey(i))
                        {
                            _clientsControl.Add(i, secondWindow);
                            secondWindow.NumClient = i;
                            _clients++;
                            break;
                        }
                    }
                   secondWindow.Show();

                    /* Add handler to detect when it's close */
                   secondWindow.ExceptionBigClient += BigClient_Event;
                   secondWindow.SwitchBigClient += SwitchBigClient;

                   Connect_Button.IsEnabled = true;
               }
               catch (Exception ee)
               {

               }
           }));
        }
        #endregion

        #region WrongPassword
        private void WrongPassword(object sender, MyEventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                errore.Content = "Password Errata";
                try {
                    balloon = new BaloonText();
                    balloon.ChangeMessage(e.Message);
                    MyNotifyIcon.ShowCustomBalloon(balloon, PopupAnimation.Slide, 4000);

                    Connect_Button.IsEnabled = true;
                }
                catch(Exception ee) {  }
            }));
        }
        #endregion

        #region Window_Closed
        private void Window_Closed(object sender, EventArgs e)
        {
            try
            {
                DeleteDirectory(@"C:\temp\");

                /* Remove handler to detect when it's close */
                foreach (KeyValuePair<int, BigClient> kvp in _clientsControl)
                {
                    kvp.Value.CloseConnection();
                    Dispatcher.BeginInvoke(new Action(() =>
                    {
                        kvp.Value.Close();
                    }));
                }
            }
            catch (Exception exc) { MessageBox.Show(exc.ToString()); }
        }
        #endregion

        #region BigClient_Event
        // Handler if there's a exception inside a BigClient or Server close connection
        private void BigClient_Event(object sender, MyEventArgs e)
        {
            // TODO: in caso di chiusura di un client, aprirne una altro se c'è
            foreach (KeyValuePair<int, BigClient> kvp in _clientsControl)
            {
                if (kvp.Value == sender)
                {
                    kvp.Value.ExceptionBigClient -= BigClient_Event;
                    kvp.Value.tokenSource.Cancel();
                    Control.ControlMouseKeyboard.UnlockButtons();
                    SwitchBigClient(sender, e);

                    kvp.Value.CloseConnection();
                    Dispatcher.BeginInvoke(new Action(() =>
                    {
                        kvp.Value.Close();
                    }));

                    _clientsControl.Remove(kvp.Key);
                    _clients--;

                    Dispatcher.BeginInvoke(new Action(() =>
                    {
                        balloon = new BaloonText();
                        if (e.Message != null)
                            balloon.ChangeMessage(e.Message);
                        else
                            balloon.ChangeMessage("Si è verificato un problema. Connessione chiusa col Server");
                        MyNotifyIcon.ShowCustomBalloon(balloon, PopupAnimation.Slide, 4000);
                    }));
                    
                    //ReorderIndexClients(kvp.Value.NumClient);
                    break;
                }
            }
        }
        #endregion

        #region CloseOtherClient
        private void CloseOtherClient()
        {
            try {
                foreach (KeyValuePair<int, BigClient> kvp in _clientsControl)
                {
                    if (kvp.Value.Visibility == Visibility.Visible)
                        kvp.Value.Visibility = Visibility.Hidden;
                }
            }
            catch(Exception e)
            {

            }
        } 
        #endregion

        #region SwitchBigClient
        private void SwitchBigClient(object sender, MyEventArgs e)
        {
            if (_clientsControl.Count > 1)
            {
                _clientsControl[int.Parse(e.NumClient)].Name = "TempDeactiveted";
                _clientsControl[int.Parse(e.NumClient)].Visibility = Visibility.Hidden;
                _clientsControl[int.Parse(e.NumClient)].WindowState = WindowState.Minimized;
                //(sender as Window).Visibility = Visibility.Hidden;
                //(sender as Window).WindowState = WindowState.Minimized;
                int client = (e.NumClient != "") ? int.Parse(e.NumClient) : int.Parse(e.Message);
                client = (_clientsControl.Count != client + 1) ? ++client : client = 0;
                while (true)
                {
                    if (_clientsControl.ContainsKey(client))
                    {
                        _clientsControl[client].Name = "WideClient";
                        _clientsControl[client].Activate();
                        _clientsControl[client].Focus();
                        _clientsControl[client].WindowState = WindowState.Maximized;
                        _clientsControl[client].ShowActivated = true;
                        _clientsControl[client].MyNotifyIcon.Visibility = Visibility.Visible;
                        _clientsControl[client].Show();
                        break;
                    }
                    else
                        client = (_clientsControl.Count != client + 1) ? ++client : client = 0;
                }
            }
        }
        #endregion

        #region DeleteDirectory
        public static void DeleteDirectory(string target_dir)
        {
            if (Directory.Exists(@"C:\temp\"))
            {
            string[] files = Directory.GetFiles(target_dir);
            string[] dirs = Directory.GetDirectories(target_dir);

            foreach (string file in files)
            {
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);
            }

            foreach (string dir in dirs)
            {
                DeleteDirectory(dir);
            }

            Directory.Delete(target_dir, false);
        }
        }
        #endregion
    }
}
