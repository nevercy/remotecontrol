﻿using System;
using System.Windows;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Input;
using System.IO;
using ObjectSerializable;
using ClipboardControl;
using System.Diagnostics;
using System.Text;
using System.Windows.Controls.Primitives; //It is important for the notification on BaloonText
using System.Runtime.InteropServices;
using Baloon;
using Event;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Windows.Interop;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;

namespace ClientSocket
{
    /* Delegato per la gestione chiusura finestra */
    public delegate void BigClientEventHandler(object sender, MyEventArgs e);

    /* Send clipboard is inside KeyDown */
    public partial class BigClient : Window
    {

        public event BigClientEventHandler ExceptionBigClient;
        public event BigClientEventHandler SwitchBigClient;

        private Socket _message;
        private Socket _senderMouse;
        private Socket _senderKeyboard;
        private Socket _senderReceiveClipboard;

        private bool _isSending = false;
        private bool _isReceving = false;

        private BaloonText _balloon;
        private ReceiveClipboard _recvClipboard;
        private SendClipboard _sendClipboard;

        private MessageStatus messageStatus;

        private int _numClient;
        private IPAddress _localAddress;

        public int NumClient
        {
            get { return _numClient; }
            set { _numClient = value; }
        }
        public IPAddress LocalAddress
        {
            get
            {
                return _localAddress;
            }

            set
            {
                _localAddress = value;
            }
        }

        private Ping _pingSender;
        public CancellationTokenSource tokenSource;

        #region GetSystemMetrics
        // TODO: <Filippo> eliminare. Esiste già in ControlMouseKeyboard
        internal enum SystemMetric
        {
            SM_CXSCREEN = 0,  /* The width of the screen of the primary display monitor, in pixels. */
            SM_CYSCREEN = 1,  /* The height of the screen of the primary display monitor, in pixels */
        }

        [DllImport("user32.dll")]
        /* Retrieves the specified system metric or system configuration setting. */
        static extern int GetSystemMetrics(SystemMetric smIndex);

        static int FindLimitCoordinateXScreen()
        {
            return 1 * (GetSystemMetrics(SystemMetric.SM_CXSCREEN));
        }

        static int FindLimitCoordinateYScreen()
        {
            return 1 * (GetSystemMetrics(SystemMetric.SM_CYSCREEN));
        }
        #endregion

        #region Init
        public BigClient()
        {
            InitializeComponent();

            KeyboardHook.WinBtnIntercepted_DOWN += KeyboardHook_WinBtnIntercepted_DOWN;
            KeyboardHook.WinBtnIntercepted_UP += KeyboardHook_WinBtnIntercepted_UP;
            IsVisibleChanged += BigClient_IsVisibleChanged;
        }

        public BigClient(Socket senderMouse, Socket senderKeyboard, Socket Sender_Receive_Clipboard, Socket status) : this()
        {
            this._senderMouse = senderMouse;
            this._senderKeyboard = senderKeyboard;
            this._senderReceiveClipboard = Sender_Receive_Clipboard;
            this._numClient = 0;
            _message = status;

            _localAddress = ((IPEndPoint)_senderMouse.LocalEndPoint).Address;

            // Set the timeout for synchronous send methods to 
            // 5 second (5000 milliseconds.)
            _senderMouse.SendTimeout = 5000;
            _senderKeyboard.SendTimeout = 5000;
            _senderReceiveClipboard.SendTimeout = 5000;
            _senderReceiveClipboard.ReceiveTimeout = 5000;
            _message.SendTimeout = 5000;

            messageStatus = new MessageStatus(ref status);

            messageStatus.MessageError += MessageError;

            _sendClipboard = new SendClipboard(_senderReceiveClipboard);
            _recvClipboard = new ReceiveClipboard(_senderReceiveClipboard);
            //NetworkChange.NetworkAddressChanged += AvailabilityChanged;

            tokenSource = new CancellationTokenSource();
            _pingSender = new Ping();

            CheckConnection();
        }
        #endregion

        #region CheckConnection
        private async void CheckConnection(){
            bool value = await SendPing();

            if (value)
                OnExceptionBigClient(new MyEventArgs("Server non raggiungibile", _numClient.ToString()));
        }
        #endregion

        #region AvailabilityChanged
        private void AvailabilityChanged(object sender, EventArgs e)
        {
            NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface n in adapters)
            {
                // TODO inserire altre interfacce
                if ((n.NetworkInterfaceType == NetworkInterfaceType.Ethernet || n.NetworkInterfaceType == NetworkInterfaceType.Wireless80211) && n.OperationalStatus == OperationalStatus.Down)
                {
                    IPInterfaceProperties ipProps = n.GetIPProperties();
                    foreach (UnicastIPAddressInformation ip in ipProps.UnicastAddresses)
                    {
                        if(ip.Address.AddressFamily == _localAddress.AddressFamily)
                            if (ip.Address.ToString() == _localAddress.ToString())
                                OnExceptionBigClient(new MyEventArgs("Problema di rete", _numClient.ToString()));
                    }
                }
            }
        }
        #endregion

        #region CloseConnection
        public void CloseConnection()
        {
            try
            {
                if (_senderMouse.Connected)
                {
                    // Disables sends and receives on a Socket. 
                    _senderMouse.Dispose();
                    _senderKeyboard.Dispose();
                    _senderReceiveClipboard.Dispose();
                    _message.Dispose();

                    //Closes the Socket connection and releases all resources 
                    _senderMouse.Close();
                    _senderKeyboard.Close();
                    _senderReceiveClipboard.Close();
                    _message.Close();
                }
            }
            catch (Exception)
            { OnExceptionBigClient(new MyEventArgs(null, _numClient.ToString())); }
        }
        #endregion

        #region RequestRemotePast
        /// <summary>
        /// Is pressed CTRL + SHIFT + V, send the local clipboard to server
        /// </summary>
        private bool IsRequestingRemotePaste
        {
            get
            {
                return (Keyboard.Modifiers == (ModifierKeys.Shift | ModifierKeys.Control) && Keyboard.IsKeyDown(Key.V));
            }
        } 
        #endregion

        #region RequestRemoteCopy
        /// <summary>
        /// If is pressed CTRL + SHIFT + C, receive the clipboard of the server.
        /// </summary>
        private bool IsRequestingRemoteCopy
        {
            get
            {
                return (Keyboard.Modifiers == (ModifierKeys.Shift | ModifierKeys.Control) && Keyboard.IsKeyDown(Key.C));
            }
        }
        #endregion

        #region InterceptWinButton_DOWN
        void KeyboardHook_WinBtnIntercepted_DOWN(object sender, WinBtnInteceptEventArgs e)
        {
            try
            {
                if (this.IsVisible && this.WindowState==WindowState.Maximized)
                {
                    Key capturedKey = e.Key;
                    SendKeyboard(EventKeyboard.KEYBOARD.KEYDOWN, e.Key);
                }
            }
            catch (Exception)
            {
                OnExceptionBigClient(new MyEventArgs(null, _numClient.ToString()));
            }
        }
        #endregion

        #region InterceptWinButton_UP
        void KeyboardHook_WinBtnIntercepted_UP(object sender, WinBtnInteceptEventArgs e)
        {
            try
            {
                if (this.IsVisible && this.WindowState == WindowState.Maximized)
                {
                    SendKeyboard(EventKeyboard.KEYBOARD.KEYUP, e.Key);
                }
            }
            catch (Exception)
            {
                OnExceptionBigClient(new MyEventArgs(null, _numClient.ToString()));
            }
        }
        #endregion

        #region MouseMove
        /// <summary>
        /// Handler that send to sever the move of the mouse.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            e.Handled = true;
            try
            {
                EventMouse mouseMove = new EventMouse(EventMouse.MOUSE.MOVE, e.GetPosition(this).X, e.GetPosition(this).Y, BigClient.FindLimitCoordinateXScreen(), BigClient.FindLimitCoordinateYScreen());
                // It is less of 256 byte
                byte[] dataBytes = SerializeToBytes(mouseMove);
                StateMouseObject stateMouse = new StateMouseObject();
                stateMouse.workSocket = _senderMouse;
                stateMouse.buffer = dataBytes;

                //if (_senderMouse.Connected)
                    _senderMouse.BeginSend(stateMouse.buffer, 0, stateMouse.buffer.Length, 0, new AsyncCallback(SendMouseCallback), stateMouse);
                //else
                   
                //    OnExceptionBigClient(new MyEventArgs("Problema di connessione", _numClient.ToString()));
            }
            catch (Exception)
            {
                OnExceptionBigClient(new MyEventArgs("Connection closed by party", _numClient.ToString()));
            }
        }
        #endregion

        #region MouseLeftButtonDown
        /// <summary>
        /// Handler that send to sever the Left Click of the mouse.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            try
            {
                EventMouse mouseMove = new EventMouse(EventMouse.MOUSE.CLICKLEFTDOWN, e.GetPosition(this).X, e.GetPosition(this).Y, BigClient.FindLimitCoordinateXScreen(), BigClient.FindLimitCoordinateYScreen());
                byte[] dataBytes = SerializeToBytes(mouseMove);
                StateMouseObject stateMouse = new StateMouseObject();
                stateMouse.workSocket = _senderMouse;


                _senderMouse.BeginSend(dataBytes, 0, dataBytes.Length, 0, new AsyncCallback(SendMouseCallback), stateMouse);
            }
            catch (Exception )
            {
                OnExceptionBigClient(new MyEventArgs(null, _numClient.ToString()));
            }
        }
        #endregion

        #region MouseLeftButtonUp
        /// <summary>
        /// Handler that send to sever the left clik up of the mouse.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            try
            {
                EventMouse mouseMove = new EventMouse(EventMouse.MOUSE.CLICKLEFTUP, e.GetPosition(this).X, e.GetPosition(this).Y, BigClient.FindLimitCoordinateXScreen(), BigClient.FindLimitCoordinateYScreen());
                byte[] dataBytes = SerializeToBytes(mouseMove);
                StateMouseObject stateMouse = new StateMouseObject();
                stateMouse.workSocket = _senderMouse;


                _senderMouse.BeginSend(dataBytes, 0, dataBytes.Length, 0, new AsyncCallback(SendMouseCallback), stateMouse);

            }
            catch (Exception )
            {
                OnExceptionBigClient(new MyEventArgs(null, _numClient.ToString()));
            }

        }
        #endregion

        #region MouseRightButtonDown
        /// <summary>
        /// Handler that send to sever the right click down of the mouse.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            try
            {
                EventMouse mouseMove = new EventMouse(EventMouse.MOUSE.CLICKRIGHTDOWN, e.GetPosition(this).X, e.GetPosition(this).Y,BigClient.FindLimitCoordinateXScreen(),BigClient.FindLimitCoordinateYScreen());
                byte[] dataBytes = SerializeToBytes(mouseMove);
                StateMouseObject stateMouse = new StateMouseObject();
                stateMouse.workSocket = _senderMouse;


                _senderMouse.BeginSend(dataBytes, 0, dataBytes.Length, 0, new AsyncCallback(SendMouseCallback), stateMouse);
            }
            catch (Exception )
            {
                OnExceptionBigClient(new MyEventArgs(null, _numClient.ToString()));
            }
        }
        #endregion

        #region MouseRightButtonUp
        /// <summary>
        /// Handler that send to sever the right click up of the mouse.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            try
            {
                EventMouse mouseMove = new EventMouse(EventMouse.MOUSE.CLICKRIGHTUP, e.GetPosition(this).X, e.GetPosition(this).Y,BigClient.FindLimitCoordinateXScreen(),BigClient.FindLimitCoordinateYScreen());
                byte[] dataBytes = SerializeToBytes(mouseMove);
                StateMouseObject stateMouse = new StateMouseObject();
                stateMouse.workSocket = _senderMouse;


                _senderMouse.BeginSend(dataBytes, 0, dataBytes.Length, 0, new AsyncCallback(SendMouseCallback), stateMouse);
            }
            catch (Exception )
            {
                OnExceptionBigClient(new MyEventArgs(null, _numClient.ToString()));
            }
        }
        #endregion

        #region MouseWheel
        /// <summary>
        /// Handler that send to sever the move of wheel of the mouse.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;

            try
            {
                EventMouse mouseMove = null;
                if(e.Delta > 0)
                    mouseMove = new EventMouse(EventMouse.MOUSE.WHEELUP, e.GetPosition(this).X, e.GetPosition(this).Y,BigClient.FindLimitCoordinateXScreen(),BigClient.FindLimitCoordinateYScreen());
                else
                    mouseMove = new EventMouse(EventMouse.MOUSE.WHEELDOWN, e.GetPosition(this).X, e.GetPosition(this).Y,BigClient.FindLimitCoordinateXScreen(),BigClient.FindLimitCoordinateYScreen());
                byte[] dataBytes = SerializeToBytes(mouseMove);
                StateMouseObject stateMouse = new StateMouseObject();
                stateMouse.workSocket = _senderMouse;


                _senderMouse.BeginSend(dataBytes, 0, dataBytes.Length, 0, new AsyncCallback(SendMouseCallback), stateMouse);
            }
            catch (Exception )
            {
                OnExceptionBigClient(new MyEventArgs(null, _numClient.ToString()));
            }
        }
        #endregion

        #region PreviewKeyDown
        /// <summary>
        /// Handler that send to sever the preview key down pressed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
            try
            {
                #region PreviewKeydown of SHIFT+CTRL+I
                // Switch the Server
                if (Keyboard.Modifiers == (ModifierKeys.Shift | ModifierKeys.Control) && Keyboard.IsKeyDown(Key.I))
                {
                    #region Empirical Adjustment of KeyUp after Shortcut

                    //The reason is because after i have switched , obviously the application
                    //is not able to send the keyUp of CTRL, and Shift
                    SendKeyboard(EventKeyboard.KEYBOARD.KEYUP, Key.LeftShift);
                    SendKeyboard(EventKeyboard.KEYBOARD.KEYUP, Key.LeftCtrl);
                    #endregion
                    MyNotifyIcon.Visibility = Visibility.Hidden;
                    OnSwitchBigClient(new MyEventArgs(_numClient.ToString(), _numClient.ToString()));
                    return;
                }
                #endregion

                #region PreviewKeydown of SHIFT+CTRL+S
                // show local Desktop
                if (Keyboard.Modifiers == (ModifierKeys.Shift | ModifierKeys.Control) && Keyboard.IsKeyDown(Key.S))
                {
                    this.WindowState = WindowState.Minimized;
                    #region Empirical Adjustment of KeyUp after Shortcut

                    //The reason is because after i have showed my desktop on client side , obviously the application
                    //is not able to send the keyUp of CTRL, because to do that is important to have the BigClient opened
                    SendKeyboard(EventKeyboard.KEYBOARD.KEYUP, Key.LeftShift);
                    SendKeyboard(EventKeyboard.KEYBOARD.KEYUP, Key.LeftCtrl);
                    #endregion
                    return;
                }
                #endregion

                #region PreviewKeydown of SHIFT+CTRL+C
                // I want receive clipboard by Server but NOT send the shortcut
                if (IsRequestingRemoteCopy)
                {
                    if (_isReceving)
                    {
                        Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(() =>
                        {
                            _balloon = new BaloonText();
                            _balloon.ChangeMessage("E' già in corso una ricezione. Attendi");
                            if (progressSend.Visibility == Visibility.Visible)
                                progressSend.Visibility = Visibility.Hidden;
                            MyNotifyIcon.ShowCustomBalloon(_balloon, PopupAnimation.Slide, 4000);
                        }));
                    }
                    else
                    {
                        // inizialization is into init()
                        _recvClipboard.ErrorReceive += ReceiveClipboard_ErrorReceive;
                        _recvClipboard.FinishReceive += ReceiveClipboard_FinishReceive;

                        SendKeyboard(EventKeyboard.KEYBOARD.REMOTECOPY, Key.C);

                        _isReceving = true;
                    }
                    return;
                }
                #endregion

                #region PreviewKeydown of SHIFT+CTRL+V
                // I want send clipboard to Server but NOT send the shortcut
                if (IsRequestingRemotePaste)
                {
                    if (_isSending)
                    {
                        Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(() =>
                        {
                            _balloon = new BaloonText();
                            _balloon.ChangeMessage("E' già in corso un invio. Attendi");
                            if (progressSend.Visibility == Visibility.Visible)
                                progressSend.Visibility = Visibility.Hidden;
                            MyNotifyIcon.ShowCustomBalloon(_balloon, PopupAnimation.Slide, 4000);
                        }));
                    }
                    else
                    {
                        /* Set the hadler to cath possible exception inside SendClipboard class */
                        _sendClipboard.ErrorSend += SendClipboard_ErrorSend;     // raised if there's an problem to send.
                        _sendClipboard.FinishSend += SendClipboard_FinishSend;   //raised when the all content of the clipboard is sent.
                        _sendClipboard.UpdateProgressBarSend += SendClipboard_UpdateProgressBar;

                        SendKeyboard(EventKeyboard.KEYBOARD.REMOTEPAST, Key.C);

                        _sendClipboard.StartSend();
                        _isSending = true;
                    }
                    return;
                }
                #endregion

                #region PreviewKeydown of SingleButton
                /* VirtualKeyFromKey = Converts a WPF Key into a Win32 Virtual-Key. */
                Key capturedKey = e.Key;
                
                #region Handle PreviewKeyDown of LeftShift with RightShift
                if (e.Key == Key.RightShift)
                {
                    capturedKey = Key.LeftShift;
                }
                #endregion

                #region Handle PreviewKeyDown of LeftCtrl with RightCtrl
                if (e.Key == Key.RightCtrl)
                {
                    capturedKey = Key.LeftCtrl;
                }
                #endregion

                #region Handle PreviewKeyDown any SystemKey (That Means Handle any Key Keydown after the Pressing of a specific Modifier)
                //It solves also the problem of left alt
                if (e.Key == Key.System)
                {
                    capturedKey = e.SystemKey;

                }
                #endregion

                SendKeyboard(EventKeyboard.KEYBOARD.KEYDOWN, capturedKey);
                #endregion
            }
            catch (Exception)
            {
                OnExceptionBigClient(new MyEventArgs(null, _numClient.ToString()));
            }
        }
        #endregion

        #region KeyUp
        /// <summary>
        /// Handler that send to sever the key up pressed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            e.Handled = true;

            try
            {
                #region keyUp of SingleButton
                /* VirtualKeyFromKey = Converts a WPF Key into a Win32 Virtual-Key. */

                Key capturedKey = e.Key;

                #region Handle KeyUp of LeftShift with RightShift 
                if ((e.Key == Key.RightShift)) {
                    capturedKey = Key.LeftShift;
                }
                #endregion

                #region Handle KeyUp of LeftCtrl with RightCtrl 
                if ((e.Key == Key.RightCtrl)) {
                    capturedKey = Key.LeftCtrl;
                }
                #endregion

                #region Handle KeyUp any SystemKey (That Means Handle any Key KeyUp after the Release of a specific Modifier)
                //It solves also the problem of Left-alt
                //and the problem of Right-alt
                if (e.Key == Key.System)
                {
                    capturedKey = e.SystemKey;

                }
                #endregion

                EventKeyboard keyboard = new EventKeyboard(EventKeyboard.KEYBOARD.KEYUP, KeyInterop.VirtualKeyFromKey(capturedKey));
                byte[] dataBytes = SerializeToBytes(keyboard);
                StateKeyboardObject stateKeyboard = new StateKeyboardObject();
                stateKeyboard.workSocket = _senderKeyboard;
                
                _senderKeyboard.BeginSend(dataBytes, 0, dataBytes.Length, 0, new AsyncCallback(SendKeyboardCallback), stateKeyboard);
                #endregion
            }
            catch (Exception )
            {
                OnExceptionBigClient(new MyEventArgs(null, _numClient.ToString()));
            }
        }
        #endregion

        #region SendMouseCallback
        /// <summary>
        /// Sent to server the event of the mouse.
        /// </summary>
        /// <param name="ar"></param>
        private void SendMouseCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                StateMouseObject stateMouse = (StateMouseObject)ar.AsyncState;
                Socket client = stateMouse.workSocket;

                // Complete sending the data to the remote device.
                int bytesSent = client.EndSend(ar);

                // se la connessione viene chiusa dal Server, qui non dovrebbe mai entrarci
                // dovrebbe essere rilanciata la stessa eccezione nella MouseMove
                if (bytesSent < 0)
                    OnExceptionBigClient(new MyEventArgs("Connection closed by party", _numClient.ToString()));

            }
            catch (Exception )
            {
                //Console.WriteLine(e.ToString());
                OnExceptionBigClient(new MyEventArgs("Error Send Mouse Operation", _numClient.ToString()));
            }
        }
        #endregion

        #region SendKeyboard
        private void SendKeyboard(EventKeyboard.KEYBOARD typeEvent, Key key)
        {
            EventKeyboard keyboard = new EventKeyboard(typeEvent, KeyInterop.VirtualKeyFromKey(key));
            byte[] dataBytes = SerializeToBytes(keyboard);
            StateKeyboardObject stateKeyboard = new StateKeyboardObject();
            stateKeyboard.workSocket = _senderKeyboard;         

            _senderKeyboard.BeginSend(dataBytes, 0, dataBytes.Length, 0, new AsyncCallback(SendKeyboardCallback), stateKeyboard);
        } 
        #endregion

        #region SendKeyboardCallback
        /// <summary>
        /// Send to server the event of the keyboard.
        /// </summary>
        /// <param name="ar"></param>
        private void SendKeyboardCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                StateKeyboardObject stateKeyboard = (StateKeyboardObject)ar.AsyncState;
                Socket client = stateKeyboard.workSocket;

                // Complete sending the data to the remote device.
                int bytesSent = client.EndSend(ar);
                if (bytesSent < 0)
                    OnExceptionBigClient(new MyEventArgs("Connection closed by party", _numClient.ToString()));

            }
            catch (Exception )
            {
                OnExceptionBigClient(new MyEventArgs("Error Send Keyboard Button", _numClient.ToString()));
            }
        }
        #endregion

        #region SerializeToBytes
        /// <summary>
        /// Serialize an object to array byte.
        /// </summary>
        /// <param name="obj"> an object that implement ISerializable</param>
        /// <returns> Array of bytes of the object</returns>
        private static byte[] SerializeToBytes(Object obj)
        {
            MemoryStream memoryStream = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(memoryStream, obj);

            return memoryStream.GetBuffer();
        }
        #endregion

        #region Handlers "SendClipboard"
        /* The two event are rised inside SendClipbord class 
         * and the handlers are added inside KeyboardDown method. */
        void SendClipboard_FinishSend(object sender, MyEventArgs e)
        {
            /* It's possible disable the handler */
            _sendClipboard.ErrorSend -= SendClipboard_ErrorSend;
            _sendClipboard.FinishSend -= SendClipboard_FinishSend;
            _sendClipboard.UpdateProgressBarSend -= SendClipboard_UpdateProgressBar;

            _isSending = false;

            if (e.Message != null)
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(() =>
                {
                    _balloon = new BaloonText();
                    _balloon.ChangeMessage(e.Message);
                    if (progressSend.Visibility == Visibility.Visible)
                        progressSend.Visibility = Visibility.Hidden;
                    MyNotifyIcon.ShowCustomBalloon(_balloon, PopupAnimation.Slide, 4000);
                }));
            }
        }

        void SendClipboard_ErrorSend(object sender, MyEventArgs e)
        {
            _sendClipboard.ErrorSend -= SendClipboard_ErrorSend;
            _sendClipboard.FinishSend -= SendClipboard_FinishSend;
            _sendClipboard.UpdateProgressBarSend -= SendClipboard_UpdateProgressBar;
            //MessageBox.Show(e.Message);
            e.NumClient = _numClient.ToString();

            //to manage the own UI
            Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(() =>
            {
                OnExceptionBigClient(e);
            }));
        }

        private void SendClipboard_UpdateProgressBar(object sender, MyEventArgs e)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(() =>
            {
                if(progressSend.Visibility == Visibility.Hidden)
                progressSend.Visibility = Visibility.Visible;
                progressSend.Value = e.ValueProgressBar;
            }));
        }
        #endregion

        #region Handlers "ReceiveClipboard"
        void ReceiveClipboard_FinishReceive(object sender, MyEventArgs e)
        {
            _recvClipboard.ErrorReceive -= ReceiveClipboard_ErrorReceive;
            _recvClipboard.FinishReceive -= ReceiveClipboard_FinishReceive;

            _isReceving = false;

            Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(() =>
            {
                _balloon = new BaloonText();
                _balloon.ChangeMessage(e.Message);
                MyNotifyIcon.ShowCustomBalloon(_balloon, PopupAnimation.Slide, 4000);
            }));
        }

        void ReceiveClipboard_ErrorReceive(object sender, MyEventArgs e)
        {
            _recvClipboard.ErrorReceive -= ReceiveClipboard_ErrorReceive;
            _recvClipboard.FinishReceive -= ReceiveClipboard_FinishReceive;
            //MessageBox.Show(e.Message);
            e.NumClient = _numClient.ToString();

            //to manage the own UI
            Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(() =>
            {
                OnExceptionBigClient(e);
            }));

        }
        #endregion

        #region Handler MessageError
        private void MessageError(object sender, MyEventArgs e)
        {
            e.NumClient = _numClient.ToString();
            Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(() =>
            {
                OnExceptionBigClient(e);
            }));
        } 
        #endregion

        #region BigClient_IsVisibleChanged
        private void BigClient_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                string message = (IsVisible) ? "Active" : "NotActive";
                messageStatus.SendMessage(message);
            }
            catch (Exception) { }
        } 
        #endregion

        #region Window_Closed
        // comunicate at SmallClient the this BigClient is closing than
        // it can remove the reference inside his Dictionary
        private void Window_Closed(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                //Delete icon
                MyNotifyIcon.Dispose();
            }));

            //OnSwitchBigClient(new MyEventArgs(_numClient.ToString()));  //When there are more then one server opened , and only one crash , the other one is maybe still alive
                                                                        //So to make the residual server controllable we have to call the OnSwitchBigClient
            OnExceptionBigClient(new MyEventArgs("Connection closed by party", _numClient.ToString()));                                                           

        }
        #endregion

        #region SendPing
        private async Task<bool> SendPing()
        {
            CancellationToken tk = tokenSource.Token;
            await Task.Run(() =>
           {
               try
               {
                   while (true)
                   {
                       // Send the ping asynchronously.
                       // Use the waiter as the user token.
                       // When the callback completes, it can wake up this thread.
                       PingReply reply = _pingSender.Send(((IPEndPoint)_message.RemoteEndPoint).Address, 10000);

                       if (reply.Status == IPStatus.Success)
                       {
                           Thread.Sleep(2000);
                       }
                       else
                       {
                           Debug.Print("Ping failed:");
                           break;
                       }
                   }
               }
               catch (Exception e)
               {
               }
           }, tk);
            return true;
        }
        #endregion

        #region OnExceptionBigClient
        protected virtual void OnExceptionBigClient(MyEventArgs e)
        {
            if (ExceptionBigClient != null)                          
                ExceptionBigClient(this, e);
        }
        #endregion

        #region OnSwitchBigClient
        protected virtual void OnSwitchBigClient(MyEventArgs e)
        {
            if (SwitchBigClient != null)
                SwitchBigClient(this, e);
        }
        #endregion

    }
}
