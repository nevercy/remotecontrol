﻿using System;
using System.Windows;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Input;
using System.IO;
using ObjectSerializable;
using System.Diagnostics;
using System.Windows.Media.Imaging;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Collections;
using ClipboardControl;

namespace ClientSocket
{
    public partial class ControlMouseAndKey : Window
    {
 
        public ControlMouseAndKey()
        {
            InitializeComponent();
        }

        // remove the state mouse
        #region MouseMove
        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            e.Handled = true;
            
            try{
                EventMouse mouseMove = new EventMouse(EventMouse.MOUSE.MOVE, e.GetPosition(this).X, e.GetPosition(this).Y);
                // It is less of 256 byte
                byte[] dataBytes = serializeToBytes(mouseMove);
                StateMouseObject stateMouse = new StateMouseObject();
                stateMouse.workSocket = Client.senderMouse;
                

                Client.senderMouse.BeginSend(dataBytes, 0, dataBytes.Length, 0, new AsyncCallback(SendMouseCallback), stateMouse);
            }
            catch (SocketException)
            {
                //MessageBox.Show("Connection closed by party");
                Window_Closed(sender, new EventArgs());
            }
        }
        #endregion

        #region MouseLeftButtonDown
        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            try
            {
                EventMouse mouseMove = new EventMouse(EventMouse.MOUSE.CLICKLEFTDOWN, e.GetPosition(this).X, e.GetPosition(this).Y);
                byte[] dataBytes = serializeToBytes(mouseMove);
                StateMouseObject stateMouse = new StateMouseObject();
                stateMouse.workSocket = Client.senderMouse;


                Client.senderMouse.BeginSend(dataBytes, 0, dataBytes.Length, 0, new AsyncCallback(SendMouseCallback), stateMouse);
            }
            catch (Exception se)
            {
                MessageBox.Show(se.ToString());
            }
        }
        #endregion

        #region MouseLeftButtonUp
        private void Window_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            try
            {
                EventMouse mouseMove = new EventMouse(EventMouse.MOUSE.CLICKLEFTUP, e.GetPosition(this).X, e.GetPosition(this).Y);
                byte[] dataBytes = serializeToBytes(mouseMove);
                StateMouseObject stateMouse = new StateMouseObject();
                stateMouse.workSocket = Client.senderMouse;


                Client.senderMouse.BeginSend(dataBytes, 0, dataBytes.Length, 0, new AsyncCallback(SendMouseCallback), stateMouse);
            }
            catch (Exception se)
            {
                MessageBox.Show(se.ToString());
            }

        }
        #endregion

        #region MouseRightButtonDown
        private void Window_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            try
            {
                EventMouse mouseMove = new EventMouse(EventMouse.MOUSE.CLICKRIGHTDOWN, e.GetPosition(this).X, e.GetPosition(this).Y);
                byte[] dataBytes = serializeToBytes(mouseMove);
                StateMouseObject stateMouse = new StateMouseObject();
                stateMouse.workSocket = Client.senderMouse;


                Client.senderMouse.BeginSend(dataBytes, 0, dataBytes.Length, 0, new AsyncCallback(SendMouseCallback), stateMouse);
            }
            catch (Exception se)
            {
                MessageBox.Show(se.ToString());
            }
        }
        #endregion

        #region MouseRightButtonUp
        private void Window_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            try
            {
                EventMouse mouseMove = new EventMouse(EventMouse.MOUSE.CLICKRIGHTUP, e.GetPosition(this).X, e.GetPosition(this).Y);
                byte[] dataBytes = serializeToBytes(mouseMove);
                StateMouseObject stateMouse = new StateMouseObject();
                stateMouse.workSocket = Client.senderMouse;


                Client.senderMouse.BeginSend(dataBytes, 0, dataBytes.Length, 0, new AsyncCallback(SendMouseCallback), stateMouse);
            }
            catch (Exception se)
            {
                MessageBox.Show(se.ToString());
            }
        }
        #endregion

        #region MouseWheel
        private void Window_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;

            try
            {
                EventMouse mouseMove = null;
                if(e.Delta > 0)
                    mouseMove = new EventMouse(EventMouse.MOUSE.WHEELUP, e.GetPosition(this).X, e.GetPosition(this).Y);
                else
                    mouseMove = new EventMouse(EventMouse.MOUSE.WHEELDOWN, e.GetPosition(this).X, e.GetPosition(this).Y);
                byte[] dataBytes = serializeToBytes(mouseMove);
                StateMouseObject stateMouse = new StateMouseObject();
                stateMouse.workSocket = Client.senderMouse;


                Client.senderMouse.BeginSend(dataBytes, 0, dataBytes.Length, 0, new AsyncCallback(SendMouseCallback), stateMouse);
            }
            catch (Exception se)
            {
                MessageBox.Show(se.ToString());
            }
        }
        #endregion

        #region KeyDown
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
            
            try
            {
                /* VirtualKeyFromKey = Converts a WPF Key into a Win32 Virtual-Key. */
                EventKeyboard keyboard = new EventKeyboard(EventKeyboard.KEYBOARD.KEYDOWN, KeyInterop.VirtualKeyFromKey(e.Key));
                byte[] dataBytes = serializeToBytes(keyboard);
                StateKeyboardObject stateKeyboard = new StateKeyboardObject();
                stateKeyboard.workSocket = Client.senderKeyboard;


                Client.senderKeyboard.BeginSend(dataBytes, 0, dataBytes.Length, 0, new AsyncCallback(SendKeyboardCallback), stateKeyboard);
            }
            catch (Exception se)
            {
                MessageBox.Show(se.ToString());
            }
        }
        #endregion

        #region KeyUp
        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            e.Handled = true;

            try
            {
                /* VirtualKeyFromKey = Converts a WPF Key into a Win32 Virtual-Key. */
                EventKeyboard keyboard = new EventKeyboard(EventKeyboard.KEYBOARD.KEYUP, KeyInterop.VirtualKeyFromKey(e.Key));
                byte[] dataBytes = serializeToBytes(keyboard);
                StateKeyboardObject stateKeyboard = new StateKeyboardObject();
                stateKeyboard.workSocket = Client.senderKeyboard;


                Client.senderKeyboard.BeginSend(dataBytes, 0, dataBytes.Length, 0, new AsyncCallback(SendKeyboardCallback), stateKeyboard);
            }
            catch (Exception se)
            {
                MessageBox.Show(se.ToString());
            }
        }
        #endregion

        #region SendMouseCallback
        private void SendMouseCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                StateMouseObject stateMouse = (StateMouseObject)ar.AsyncState;
                Socket client = stateMouse.workSocket;

                // Complete sending the data to the remote device.
                int bytesSent = client.EndSend(ar);
                //Debug.Print("Sent {0} bytes to server.", bytesSent);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
        #endregion

        #region SendKeyboardCallback
        private void SendKeyboardCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                StateKeyboardObject stateKeyboard = (StateKeyboardObject)ar.AsyncState;
                Socket client = stateKeyboard.workSocket;

                // Complete sending the data to the remote device.
                int bytesSent = client.EndSend(ar);
                //Debug.Print("Sent {0} bytes to server.", bytesSent);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
        #endregion

        #region serializeToBytes
        private static byte[] serializeToBytes(Object obj)
        {
            MemoryStream memoryStream = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(memoryStream, obj);

            return memoryStream.GetBuffer();
        }
        #endregion

        #region Closing
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string msg = "Are you sure to close control?";
            MessageBoxResult result =
              MessageBox.Show(
                msg,
                "Data App",
                MessageBoxButton.YesNo,
                MessageBoxImage.Warning);
            if (result == MessageBoxResult.No)
            {
                // If user doesn't want to close, cancel closure
                e.Cancel = true;
            }
            else
                Window_Closed(sender, e);
            
        }
        #endregion

        #region Closed
        private void Window_Closed(object sender, EventArgs e)
        {
           
        }
        #endregion 

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SendClipboard.SendClip(Client.Sender_Receive_Clipboard);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ReceiveClipboard.ReceiveClip(Client.Sender_Receive_Clipboard);
        }

    }
}
