﻿using System;
using System.Runtime.Serialization;

namespace ServerSocketWpfApp
{
    [Serializable]
    public class ObjectStream : ISerializable
    {
        public string message;

        // Implement this method to serialize data. The method is called  
        // on serialization. 

        public string getMessage()
        {
            return this.message;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {

        }
        protected ObjectStream(SerializationInfo info, StreamingContext context)
        {

            this.message = (string)info.GetValue("message", typeof(string));
        }

    }
}
