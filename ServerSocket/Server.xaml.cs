﻿using System;
using System.Text;
using System.Windows;
using System.Net;
using System.Net.Sockets;
using ObjectSerializable;
using Control;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Diagnostics;
using ClipboardControl;
using System.Windows.Controls.Primitives; //It is important for the notification on BaloonText
using MahApps.Metro.Controls;
using IconTray;
using Event;
using System.Windows.Threading;
using System.Text.RegularExpressions;
using System.Net.NetworkInformation;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.Generic;

namespace ServerSocket
{
    public delegate void ServerEventHandler(object sender, MyEventArgs args);
    public partial class Server : MetroWindow
    {
        public static event ServerEventHandler UpdateProgressBar_onBaloon;

        private Socket _listenerMouse;
        private Socket _handlerMouse;
        private Socket _listenerKeyboard;
        private Socket _handlerKeyboard;
        private Socket _listenerClipboard;
        private Socket _handlerClipboard;
        private Socket _listenerMessage;
        private Socket _handlerMessage;

        private BaloonText _balloon;
        private BorderWindow _borderWindow;
        private ReceiveClipboard _recvClipboard;
        private SendClipboard _sendClipboard;

        private string _password;
        private int _portMessage;
        private int _portMouse;
        private int _portKeyboard;
        private int _portClipboard;
        string _selectedInterface;

        private Ping _pingSender;
        private Task _pingTask;

        private IPAddress _localAddress;

        CancellationTokenSource tokenSource;
        NetworkInterface[] adapters;

        private event ServerEventHandler ErrorServer;
        private bool _isBaloonTextActive = false;


        #region Init
        public Server()
        {
            InitializeComponent();

            _password = Properties.Settings.Default.password;
            _portMessage = Properties.Settings.Default.portMessage;
            _portMouse = Properties.Settings.Default.portMouse;
            _portKeyboard = Properties.Settings.Default.portKeyboard;
            _portClipboard = Properties.Settings.Default.portClipboard;
            
            _balloon = new BaloonText();
            _balloon.ChangeMessage("Welcome");
            //show balloon and close it after 2 seconds
            MyNotifyIcon.ShowCustomBalloon(_balloon, PopupAnimation.Slide, 4000);

            _borderWindow = new BorderWindow();
            _borderWindow.Hide();
            Start_Button.IsEnabled = true;

            if (!Directory.Exists(@"C:\temp\"))
                Directory.CreateDirectory(@"C:\temp\");

            ShowMessageCommand.ShowWindowEvent += ManageIconTray;
            NetworkChange.NetworkAddressChanged += AvailabilityChanged;

            adapters = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface n in adapters)
            {
                if ((n.NetworkInterfaceType == NetworkInterfaceType.Ethernet) || (n.NetworkInterfaceType == NetworkInterfaceType.Wireless80211))
                    if(!n.Description.Contains("Virtual"))
                        interfaces.Items.Add(n.Description);
            }
        }
        #endregion

        #region AvailabilityChanged
        private void AvailabilityChanged(object sender, EventArgs e)
        {
            NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface n in adapters)
            {
                if ((n.NetworkInterfaceType == NetworkInterfaceType.Ethernet || n.NetworkInterfaceType == NetworkInterfaceType.Wireless80211) && n.OperationalStatus == OperationalStatus.Down)
                    OnErrorServer(new MyEventArgs("Problema di rete"));
            }
        }
        #endregion

        #region ManageIconTray
        //This code is used to handle the visibility of the window
        //The handle of the event is activated and registered at the startup of the Server
        //How you can see the visibility is changed across the method Hide of the window
        //The event is sended to the .NET by clicking the context Menu on the icon tray
        void ManageIconTray(object s, EventArgs e)
        {
            try {
                IconEventArgs iea = (IconEventArgs)e;
                if (iea.Type == "reopen")
                    ChangeVisibility();
                else
                {
                    OnErrorServer(new MyEventArgs("Bye"));
                    Close();
                }
            }
            catch(Exception ) {  }
        }
        #endregion

        #region ChangeVisibility
        void ChangeVisibility()
        {
            if (Visibility == Visibility.Hidden || this.WindowState == WindowState.Minimized)
            {
                Show();
                this.WindowState = System.Windows.WindowState.Normal;
            }
        }
        #endregion

        #region StartClick
        private void Start_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ErrorServer += Server_ErrorServer;
                _selectedInterface = interfaces.Text;
                 _localAddress = IPAddress.Any;
                if (CheckInterface())
                {
                    if (CheckPort())
                    {
                        if (CheckPassword())
                        {
                            /* Creates a network endpoint */
                            IPEndPoint ipEndPointM = new IPEndPoint(_localAddress, _portMouse);
                            IPEndPoint ipEndPointK = new IPEndPoint(_localAddress, _portKeyboard);
                            IPEndPoint ipEndPointC = new IPEndPoint(_localAddress, _portClipboard);
                            IPEndPoint ipEndPointS = new IPEndPoint(_localAddress, _portMessage);

                            /* Create one Socket object to listen the incoming connection */
                            _listenerMouse = new Socket(
                                AddressFamily.InterNetwork,
                                SocketType.Stream,
                                ProtocolType.Tcp
                                );

                            _listenerKeyboard = new Socket(
                               AddressFamily.InterNetwork,
                               SocketType.Stream,
                               ProtocolType.Tcp
                               );

                            _listenerClipboard = new Socket(
                               AddressFamily.InterNetwork,
                               SocketType.Stream,
                               ProtocolType.Tcp
                               );

                            _listenerMessage = new Socket(
                               AddressFamily.InterNetwork,
                               SocketType.Stream,
                               ProtocolType.Tcp
                               );

                            /* Disable Nagle algorithm */
                            _listenerMouse.NoDelay = true;
                            _listenerKeyboard.NoDelay = true;
                            _listenerClipboard.NoDelay = true;
                            _listenerMessage.NoDelay = true;

                            /* Associates a Socket with a local endpoint */
                            _listenerMouse.Bind(ipEndPointM);
                            _listenerKeyboard.Bind(ipEndPointK);
                            _listenerClipboard.Bind(ipEndPointC);
                            _listenerMessage.Bind(ipEndPointS);

                            /* Places a Socket in a listening state and specifies the maximum 
                               Length of the pending connections queue */
                            _listenerMouse.Listen(0);
                            _listenerKeyboard.Listen(0);
                            _listenerClipboard.Listen(0);
                            _listenerMessage.Listen(0);

                            /* Begins an asynchronous operation to accept an attempt */
                            _listenerMouse.BeginAccept(new AsyncCallback(AcceptMouseCallback), _listenerMouse);
                            _listenerKeyboard.BeginAccept(new AsyncCallback(AcceptKeyboardCallback), _listenerKeyboard);
                            _listenerClipboard.BeginAccept(new AsyncCallback(AcceptClipboardCallback), _listenerClipboard);
                            _listenerMessage.BeginAccept(new AsyncCallback(AcceptStatusCallback), _listenerMessage);

                            Start_Button.IsEnabled = false;
                            _balloon = new BaloonText();
                            _balloon.ChangeMessage("Server is listening at port " + _portMessage);
                            MyNotifyIcon.ShowCustomBalloon(_balloon, PopupAnimation.Slide, 4000);

                            localStatus.Content = "Server is listening at port " + _portMessage + System.Environment.NewLine + "With Address " + _localAddress;
                        }
                    }
                }
            }
            catch (Exception exc) { OnErrorServer(new MyEventArgs(exc.ToString())); }
        }
        #endregion

        #region CheckInterface
        private bool CheckInterface()
        {
            if (!interfaces.Text.Equals(""))
            {
                foreach (NetworkInterface n in adapters)
                {
                    if (n.Description == _selectedInterface)
                    {
                        if (n.Speed != -1)
                        {
                            IPInterfaceProperties ipProps = n.GetIPProperties();
                            foreach (UnicastIPAddressInformation ip in ipProps.UnicastAddresses)
                            {
                                if (ip.Address.AddressFamily == AddressFamily.InterNetwork)
                                {
                                    _localAddress = ip.Address;
                                    return true;
                                }
                            }
                        }
                        else
                        {
                            localStatus.Content = "L'interfaccia selezionata è scollegata";
                            return false;
                        }
                    }
                }
            }
            else
            {
                localStatus.Content = "Selezionare un interfaccia";
                return false;
            }
            return false;
        } 
        #endregion

        #region CheckPort
        private bool CheckPort()
        {
            try
            {
                if (!portToListen.Text.Equals(""))
                {
                    bool isNumber = IsNumberAllowed(portToListen.Text);

                    if (!isNumber)
                    {
                        localStatus.Content = "Insert a valid port numer";
                        return false;
                    }
                    else
                    {
                        int selectedPort = Int32.Parse(portToListen.Text.ToString());

                        if (selectedPort < 1024 || selectedPort > 65535)
                        {
                            localStatus.Content = "Insert a port between 1025 and 65535";
                            return false;
                        }
                        else
                        {
                            _portMessage = selectedPort;
                            if (ScanAndSetFreePorts(selectedPort))
                                return true;
                            else
                            {
                                localStatus.Content = "La porta " + selectedPort + " selezionata è occupata";
                                return false;
                            }
                        }
                    }
                }
                else
                {
                    localStatus.Content = "Inserire una porta";
                    return false;
                }
            }
            catch (Exception )
            {
                localStatus.Content = "Insert a port between 1025 and 65535";
                return false;
            }
        }
        #endregion

        #region CheckPassword
        private bool CheckPassword()
        {
            if (!password.Password.Equals(""))
            {
                _password = password.Password;
                return true;
            }
            else
            {
                localStatus.Content = "Inserire password";
                return false;
            }
        }
        #endregion

        #region ScanAndSetFreePorts
        private bool ScanAndSetFreePorts(int selectedPort)
        {
            IPGlobalProperties ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();
            TcpConnectionInformation[] tcpConnInfoArray = ipGlobalProperties.GetActiveTcpConnections();

            List<int> randomNumbers = new List<int>();
            List<int> busyPorts = new List<int>();

            foreach (TcpConnectionInformation tcpi in tcpConnInfoArray)
            {
                busyPorts.Add(tcpi.LocalEndPoint.Port);
            }

            if (busyPorts.Contains(_portMessage))
            {
                return false;
            }

            Random random = new Random();
            for (int i = 0; i < 3; i++)
            {
                int number;

                do number = random.Next(1025, 65534);
                while (randomNumbers.Contains(number) && number != _portMessage && !busyPorts.Contains(number));

                randomNumbers.Add(number);
            }

            _portMouse = randomNumbers[0];
            _portKeyboard = randomNumbers[1];
            _portClipboard = randomNumbers[2];

            return true;
        }
        #endregion

        #region IsNumberAllowed
        private static bool IsNumberAllowed(string text)
        {
            Regex regex = new Regex(@"^[0-9]+$"); //regex that matches disallowed text
            return regex.IsMatch(text);
        } 
        #endregion

        #region AcceptMouseCallback
        public void AcceptMouseCallback(IAsyncResult ar)
        {
            try
            {
                // Get Listening Socket object 
                Socket listener = (Socket)ar.AsyncState;
                // Create a new socket to handle a client
                _handlerMouse = listener.EndAccept(ar);
                // not want to allow another connection
                listener.Close();

                _handlerMouse.ReceiveTimeout = 5000;

                StateMouseObject stateMouse = new StateMouseObject();
                stateMouse.workSocket = _handlerMouse;
                stateMouse.toReceive = StateMouseObject.BufferSize;

                // Begins to asynchronously receive data 
                _handlerMouse.BeginReceive(
                    stateMouse.buffer,          // An array of type Byt for received data 
                    0,                          // The zero-based position in the buffer  
                    StateMouseObject.BufferSize,// The number of bytes to receive 
                    SocketFlags.None,           // Specifies send and receive behaviors 
                    new AsyncCallback(ReceiveMouseCallback),//An AsyncCallback delegate 
                    stateMouse            // Specifies infomation for receive operation 
                    );                
            }
            catch (Exception exc) {
                OnErrorServer(new MyEventArgs(exc.ToString()));
            }
        }
        #endregion

        #region ReceiveMouseCallback
        public void ReceiveMouseCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the handler socket
                // from the asynchronous state object.
                StateMouseObject stateMouseObject = (StateMouseObject)ar.AsyncState;
                Socket handler = stateMouseObject.workSocket;

                // Read data from the client socket. 
                int bytesRead = handler.EndReceive(ar);

                if (bytesRead > 0)
                {
                    stateMouseObject.toReceive -= bytesRead;

                    if (stateMouseObject.toReceive == 0)
                    {
                        if (stateMouseObject.ms != null)
                        {
                            EventMouseReceived(stateMouseObject.ms.GetBuffer());
                        }
                        else
                        {
                            EventMouseReceived(stateMouseObject.buffer);
                        }
                            

                        StateMouseObject newStateMouseObject = new StateMouseObject();
                        newStateMouseObject.workSocket = handler;
                        newStateMouseObject.toReceive = StateMouseObject.BufferSize;

                        // Begins to asynchronously receive data 
                        handler.BeginReceive(
                            newStateMouseObject.buffer,         // An array of type Byte for received data 
                            0,                                  // The zero-based position in the buffer  
                            StateMouseObject.BufferSize,        // The number of bytes to receive 
                            SocketFlags.None,                   // Specifies send and receive behaviors 
                            new AsyncCallback(ReceiveMouseCallback), //An AsyncCallback delegate 
                            newStateMouseObject                 // Specifies infomation for receive operation 
                            );
                    }
                    else
                    {
                        stateMouseObject.ms = new MemoryStream();
                        // Transfer the imcomplete object into the steam.
                        stateMouseObject.ms.Write(stateMouseObject.buffer, 0, bytesRead);
                        // Create a new buffer ready for new incoming bytes.
                        stateMouseObject.buffer = new byte[StateMouseObject.BufferSize];
                        handler.BeginReceive(
                            stateMouseObject.buffer,            // An array of type Byt for received data 
                            0,                                  // The zero-based position in the buffer  
                            StateMouseObject.BufferSize,        // The number of bytes to receive 
                            SocketFlags.None,                   // Specifies send and receive behaviors 
                            new AsyncCallback(ReceiveMouseCallback), //An AsyncCallback delegate 
                            stateMouseObject                    // Specifies infomation for receive operation 
                            );
                    }
                }
                else
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        handler.Close();
                        OnErrorServer(new MyEventArgs(null));
                    }));
                }
            }
            catch (Exception exc) 
            {
                OnErrorServer(new MyEventArgs(exc.ToString()));
            }
        }
        #endregion

        #region EventMouseReceived
        private void EventMouseReceived(byte[] buffer)
        {
            try {
                EventMouse eventUman = (EventMouse)DeserializeFromBytes(buffer);


                switch (eventUman.typeEvent)
                {
                    case EventMouse.MOUSE.MOVE:

                        ControlMouseKeyboard.MouseMove((int)eventUman.PX, (int)eventUman.PY, (int)eventUman.XScreenResolution, (int)eventUman.YScreenResolution);
                        break;

                    case EventMouse.MOUSE.CLICKLEFTDOWN:
                        ControlMouseKeyboard.LeftClickDown((int)eventUman.PX, (int)eventUman.PY, (int)eventUman.XScreenResolution, (int)eventUman.YScreenResolution);
                        break;

                    case EventMouse.MOUSE.CLICKLEFTUP:
                        ControlMouseKeyboard.LeftClickUp((int)eventUman.PX, (int)eventUman.PY, (int)eventUman.XScreenResolution, (int)eventUman.YScreenResolution);
                        break;

                    case EventMouse.MOUSE.CLICKRIGHTDOWN:
                        ControlMouseKeyboard.RightClickDown((int)eventUman.PX, (int)eventUman.PY, (int)eventUman.XScreenResolution, (int)eventUman.YScreenResolution);
                        break;

                    case EventMouse.MOUSE.CLICKRIGHTUP:
                        ControlMouseKeyboard.RightClickUp((int)eventUman.PX, (int)eventUman.PY, (int)eventUman.XScreenResolution, (int)eventUman.YScreenResolution);
                        break;

                    case EventMouse.MOUSE.WHEELUP:
                        ControlMouseKeyboard.MouseWheelUp((int)eventUman.PX, (int)eventUman.PY, (int)eventUman.XScreenResolution, (int)eventUman.YScreenResolution);
                        break;

                    case EventMouse.MOUSE.WHEELDOWN:
                        ControlMouseKeyboard.MouseWheelDown((int)eventUman.PX, (int)eventUman.PY, (int)eventUman.XScreenResolution, (int)eventUman.YScreenResolution);
                        break;
                }
            }
            catch (Exception)
            {
                OnErrorServer(new MyEventArgs(null));
            }

        }
        #endregion

        #region AcceptKeyboardCallback
        public void AcceptKeyboardCallback(IAsyncResult ar)
        {
            try
            {
                // Get Listening Socket object 
                Socket listener = (Socket)ar.AsyncState;
                // Create a new socket to handle a client
                _handlerKeyboard = listener.EndAccept(ar);
                // not want to allow another connection
                listener.Close();

                _handlerKeyboard.ReceiveTimeout = 5000;

                StateKeyboardObject stateKeyboard = new StateKeyboardObject();
                stateKeyboard.workSocket = _handlerKeyboard;
                stateKeyboard.toReceive = StateKeyboardObject.BufferSize;

                // Begins to asynchronously receive data 
                _handlerKeyboard.BeginReceive(
                    stateKeyboard.buffer,          // An array of type Byt for received data 
                    0,                          // The zero-based position in the buffer  
                    StateKeyboardObject.BufferSize,// The number of bytes to receive 
                    SocketFlags.None,           // Specifies send and receive behaviors 
                    new AsyncCallback(ReceiveKeyboardCallback),//An AsyncCallback delegate 
                    stateKeyboard            // Specifies infomation for receive operation 
                    );
            }
            catch (Exception exc)
            {
                OnErrorServer(new MyEventArgs(null));
            }
        }
        #endregion

        #region ReceiveKeyboardCallback
        public void ReceiveKeyboardCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the handler socket
                // from the asynchronous state object.
                StateKeyboardObject stateKeyboardObject = (StateKeyboardObject)ar.AsyncState;
                Socket handler = stateKeyboardObject.workSocket;

                // Read data from the client socket. 
                int bytesRead = handler.EndReceive(ar);

                if (bytesRead > 0)
                {
                    stateKeyboardObject.toReceive -= bytesRead;

                    if (stateKeyboardObject.toReceive == 0)
                    {
                        StateKeyboardObject newStateKeyboardObject = new StateKeyboardObject();
                        newStateKeyboardObject.workSocket = handler;
                        newStateKeyboardObject.toReceive = StateKeyboardObject.BufferSize;
                        if (stateKeyboardObject.ms != null)
                            EventKeyboardReceived(stateKeyboardObject.ms.GetBuffer());
                        else
                            EventKeyboardReceived(stateKeyboardObject.buffer);

                        // Begins to asynchronously receive data 
                        handler.BeginReceive(
                            newStateKeyboardObject.buffer,         // An array of type Byte for received data 
                            0,                                  // The zero-based position in the buffer  
                            StateKeyboardObject.BufferSize,        // The number of bytes to receive 
                            SocketFlags.None,                   // Specifies send and receive behaviors 
                            new AsyncCallback(ReceiveKeyboardCallback), //An AsyncCallback delegate 
                            newStateKeyboardObject                 // Specifies infomation for receive operation 
                            );
                    }
                    else
                    {
                        stateKeyboardObject.ms = new MemoryStream();
                        // Transfer the imcomplete object into the steam.
                        stateKeyboardObject.ms.Write(stateKeyboardObject.buffer, 0, bytesRead);
                        // Create a new buffer ready for new incoming bytes.
                        stateKeyboardObject.buffer = new byte[StateMouseObject.BufferSize];
                        handler.BeginReceive(
                            stateKeyboardObject.buffer,            // An array of type Byt for received data 
                            0,                                  // The zero-based position in the buffer  
                            StateKeyboardObject.BufferSize,        // The number of bytes to receive 
                            SocketFlags.None,                   // Specifies send and receive behaviors 
                            new AsyncCallback(ReceiveKeyboardCallback), //An AsyncCallback delegate 
                            stateKeyboardObject                    // Specifies infomation for receive operation 
                            );
                    }
                }
                else
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        handler.Close();
                    }));
                }
            }
            catch (Exception exc)
            {
                OnErrorServer(new MyEventArgs(exc.ToString()));
            }
        }
        #endregion

        #region EventKeyboardReceived
        private void EventKeyboardReceived(byte[] buffer)
        {
            try {
                EventKeyboard eventUman = (EventKeyboard)DeserializeFromBytes(buffer);
                #region CombinazioniNonFunzionanti
                //ALT+INVIO = Visualizza proprietà di un elemento
                #endregion
                switch (eventUman.typeEvent)
                {
                    case EventKeyboard.KEYBOARD.KEYDOWN:
                        Key k = KeyInterop.KeyFromVirtualKey(eventUman.KEY);
                        ControlMouseKeyboard.KeyDown(eventUman.KEY); //162
                        break;

                    case EventKeyboard.KEYBOARD.KEYUP:
                        ControlMouseKeyboard.KeyUp(eventUman.KEY);
                        break;

                    case EventKeyboard.KEYBOARD.REMOTECOPY:
                        Debug.Print("REMOTE COPY");
                        _sendClipboard.ErrorSend += SendClipboard_ErrorSend;     // raised if there's an problem to send.
                        _sendClipboard.FinishSend += SendClipboard_FinishSend;   //raised when the all content of the clipboard is sent.
                        _sendClipboard.UpdateProgressBarSend += SendClipboard_UpdateProgressBar;
                        _sendClipboard.StartSend();

                        Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(() =>
                        {
                            _balloon = new BaloonText();
                            _isBaloonTextActive = true;
                            _balloon.ChangeMessage("PERCENTAGE OF TRANSFER");
                            MyNotifyIcon.ShowCustomBalloon(_balloon, PopupAnimation.Slide, null);
                        }));
                        break;

                    case EventKeyboard.KEYBOARD.REMOTEPAST:
                        Debug.Print("REMOTE PAST");
                        _recvClipboard.ErrorReceive += ReceiveClipboard_ErrorReceive;
                        _recvClipboard.FinishReceive += ReceiveClipboard_FinishReceive;
                        break;
                }
            }
            catch
            {
                OnErrorServer(new MyEventArgs(null));
            }

        }
        #endregion

        #region AcceptClipboardCallback
        public void AcceptClipboardCallback(IAsyncResult ar)
        {
            try {
                // Get Listening Socket object 
                Socket listener = (Socket)ar.AsyncState;
                // Create a new socket to handle a client
                _handlerClipboard = listener.EndAccept(ar);
                // not want to allow another connection
                listener.Close();

                _handlerClipboard.ReceiveTimeout = 5000;
                _handlerClipboard.SendTimeout = 5000;

                _recvClipboard = new ReceiveClipboard(_handlerClipboard);
                _sendClipboard = new SendClipboard(_handlerClipboard);
            }
            catch (Exception)
            {
                OnErrorServer(new MyEventArgs(null));
            }
        }
        #endregion

        #region AcceptStatusCallback
        public void AcceptStatusCallback(IAsyncResult ar)
        {
            try {
                // Get Listening Socket object 
                Socket listener = (Socket)ar.AsyncState;
                // Create a new socket to handle a client
                _handlerMessage = listener.EndAccept(ar);
                // not want to allow another connection
                listener.Close();

                _handlerMessage.SendTimeout = 5000;
                _handlerMessage.ReceiveTimeout = 5000;

                StateStatusObject stateStatus = new StateStatusObject();
                stateStatus.workSocket = _handlerMessage;
                stateStatus.toReceive = StateStatusObject.BufferSize;

                // Begins to asynchronously receive data 
                _handlerMessage.BeginReceive(
                    stateStatus.buffer,          // An array of type Byt for received data 
                    0,                          // The zero-based position in the buffer  
                    StateStatusObject.BufferSize,// The number of bytes to receive 
                    SocketFlags.None,           // Specifies send and receive behaviors 
                    new AsyncCallback(ReceiveStatusCallback),//An AsyncCallback delegate 
                    stateStatus            // Specifies infomation for receive operation 
                    );
            }
            catch(Exception)
            {
                OnErrorServer(new MyEventArgs(null));
            }
        }
        #endregion

        #region ReceiveStatusCallback
        public void ReceiveStatusCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the handler socket
                // from the asynchronous state object.
                StateStatusObject stateStatusObject = (StateStatusObject)ar.AsyncState;
                Socket handler = stateStatusObject.workSocket;

                // Read data from the client socket. 
                int bytesRead = handler.EndReceive(ar);

                if (bytesRead > 0)
                {
                    //stateStatusObject.toReceive -= bytesRead;
                    stateStatusObject.toReceive -= 256;

                    if (stateStatusObject.toReceive == 0)
                    {
                        StateStatusObject newStateStatusObject = new StateStatusObject();
                        newStateStatusObject.workSocket = handler;
                        newStateStatusObject.toReceive = StateStatusObject.BufferSize;

                        if (stateStatusObject.ms != null)
                            EventStatusReceived(stateStatusObject.ms.GetBuffer(), 256);
                        else
                            EventStatusReceived(stateStatusObject.buffer, bytesRead);

                        // Begins to asynchronously receive data 
                        handler.BeginReceive(
                            newStateStatusObject.buffer,         // An array of type Byte for received data 
                            0,                                  // The zero-based position in the buffer  
                            StateStatusObject.BufferSize,        // The number of bytes to receive 
                            SocketFlags.None,                   // Specifies send and receive behaviors 
                            new AsyncCallback(ReceiveStatusCallback), //An AsyncCallback delegate 
                            newStateStatusObject                 // Specifies infomation for receive operation 
                            );
                    }
                    else
                    {
                        stateStatusObject.ms = new MemoryStream();
                        // Transfer the imcomplete object into the steam.
                        stateStatusObject.ms.Write(stateStatusObject.buffer, 0, bytesRead);
                        // Create a new buffer ready for new incoming bytes.
                        stateStatusObject.buffer = new byte[StateStatusObject.BufferSize];
                        handler.BeginReceive(
                            stateStatusObject.buffer,            // An array of type Byt for received data 
                            0,                                  // The zero-based position in the buffer  
                            StateStatusObject.BufferSize,        // The number of bytes to receive 
                            SocketFlags.None,                   // Specifies send and receive behaviors 
                            new AsyncCallback(ReceiveStatusCallback), //An AsyncCallback delegate 
                            stateStatusObject                    // Specifies infomation for receive operation 
                            );
                    }
                }
                else
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        handler.Close();
                    }));
                }
            }
            catch (Exception exc)
            {
                OnErrorServer(new MyEventArgs(exc.ToString()));
            }
        }
        #endregion

        #region SendStatusCallback
        public void SendStatusCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket client = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.
                int bytesSent = client.EndSend(ar);
                if (bytesSent < 0)
                    OnErrorServer(new MyEventArgs("Connection closed by party"));

                //Debug.Print("Sent {0} bytes to server.", bytesSent);

                // Signal that all bytes have been sent.
                //sendDone.Set();
            }
            catch (Exception)
            {
                OnErrorServer(new MyEventArgs("Error send _message to the server"));
            }
        }
        #endregion

        #region EventStatusReceived
        private void EventStatusReceived(byte[] data, int dataRead)
        {
            try {
                String content = String.Empty;
                StringBuilder sb = new StringBuilder();
                sb.Append(Encoding.ASCII.GetString(data, 0, dataRead));

                content = sb.ToString();

                switch (content)
                {
                    case "NotActive":
                        Debug.Print("NotActive");
                        Dispatcher.BeginInvoke(new Action(() => { _borderWindow.Hide(); }));
                        break;
                    case "Active":
                        Debug.Print("Active");
                        Dispatcher.BeginInvoke(new Action(() => { _borderWindow.Show(); }));
                        break;
                    default:
                        CheckPassword(content);
                        break;
                };
            }
            catch (Exception)
            {
                OnErrorServer(new MyEventArgs(null));
            }
        }
        #endregion

        #region CheckPassword
        private void CheckPassword(string pass)
        {
            try {
                byte[] msg;
                if (!pass.Equals(_password))
                {
                    OnErrorServer(new MyEventArgs("Wrong Password"));
                    msg = Encoding.ASCII.GetBytes("WrongPassword");
                }
                else
                {
                    msg = Encoding.ASCII.GetBytes("OkPassword " +_portMouse+" "+_portKeyboard+" "+_portClipboard);

                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        Hide(); //Command to disable the visibility of the window , exactly when the connection with the client was established
                        _balloon = new BaloonText();
                        _balloon.ChangeMessage("Connected with client " + _handlerMessage.RemoteEndPoint);
                        localStatus.Content = "Connected with client " + _handlerMessage.RemoteEndPoint;
                        MyNotifyIcon.ShowCustomBalloon(_balloon, PopupAnimation.Slide, 4000);
                    }));
                    _pingSender = new Ping();
                    tokenSource = new CancellationTokenSource();
                    CancellationToken ct = tokenSource.Token;
                    _pingTask = Task.Run(() => SendPing(), ct);
                }

                _handlerMessage.BeginSend(msg, 0, msg.Length, 0, new AsyncCallback(SendStatusCallback), _handlerMessage);
            }
            catch (Exception) {
                OnErrorServer(new MyEventArgs(null));
            }
        } 
        #endregion

        #region Handlers "ReceiveClipboard"
        /* The two event are rised inside SendClipbord class 
        * and the handlers are added inside KeyboardDown method. */
        void ReceiveClipboard_FinishReceive(object sender, MyEventArgs e)
        {
            if (_isBaloonTextActive)
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    _balloon = new BaloonText();
                    _balloon.ChangeMessage(e.Message);
                    MyNotifyIcon.ShowBalloonTip("", e.Message, MyNotifyIcon.Icon);
                    _isBaloonTextActive = false;
                }));
            } else
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    _balloon = new BaloonText();
                    _balloon.ChangeMessage(e.Message);
                    MyNotifyIcon.ShowCustomBalloon(_balloon, PopupAnimation.Slide, 4000);
                }));
            }
            

            _recvClipboard.ErrorReceive -= ReceiveClipboard_ErrorReceive;
            _recvClipboard.FinishReceive -= ReceiveClipboard_FinishReceive;
        }

        void ReceiveClipboard_ErrorReceive(object sender, MyEventArgs e)
        { 
            _recvClipboard.ErrorReceive -= ReceiveClipboard_ErrorReceive;
            _recvClipboard.FinishReceive -= ReceiveClipboard_FinishReceive;
            //MessageBox.Show(e.Message);
            OnErrorServer(e);
        }  
        #endregion

        #region Handlers "SendClipboard"
        /* The two event are rised inside SendClipbord class 
         * and the handlers are added inside KeyboardDown method. */
        void SendClipboard_FinishSend(object sender, MyEventArgs e)
        {
            _isBaloonTextActive = false;
            Dispatcher.BeginInvoke(new Action(() =>
            {
                _balloon = new BaloonText();
                _balloon.ChangeMessage(e.Message);
                MyNotifyIcon.ShowCustomBalloon(_balloon, PopupAnimation.Slide, 4000);
            }));
            /* It's possible disable the handler */
            _sendClipboard.ErrorSend -= SendClipboard_ErrorSend;
            _sendClipboard.FinishSend -= SendClipboard_FinishSend;
            _sendClipboard.UpdateProgressBarSend -= SendClipboard_UpdateProgressBar;
        }

        void SendClipboard_ErrorSend(object sender, MyEventArgs e)
        {
            _isBaloonTextActive = false;
            _sendClipboard.ErrorSend -= SendClipboard_ErrorSend;
            _sendClipboard.FinishSend -= SendClipboard_FinishSend;
            _sendClipboard.UpdateProgressBarSend -= SendClipboard_UpdateProgressBar;

            OnErrorServer(e);
        }

        private void SendClipboard_UpdateProgressBar(object sender, MyEventArgs e)
        {
            double value = e.ValueProgressBar;
            OnUpdateProgressBar(new MyEventArgs(value));
        }
        #endregion

        #region Handler ErrorServer
        private void Server_ErrorServer(object sender, MyEventArgs args)
        {
            try {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    this.ErrorServer -= Server_ErrorServer;
                    if (tokenSource != null)
                        tokenSource.Cancel();
                    ControlMouseKeyboard.UnlockButtons();

                    _balloon = new BaloonText();
                    if (args.Message != null)
                        _balloon.ChangeMessage(args.Message);
                    else
                        _balloon.ChangeMessage("Si è verificato un problema. Connessione chiusa col Client");
                    MyNotifyIcon.ShowCustomBalloon(_balloon, PopupAnimation.Slide, 4000);

                //MessageBox.Show(args.Message);

                if(args.Message != "Bye")
                    CloseConnection();
                }));
            }
            catch(Exception e)
            {

            }
        }
        #endregion

        #region DeserializeFromBytes
        private static Object DeserializeFromBytes(byte[] buffer)
        {
            MemoryStream ms = new MemoryStream(buffer);
            BinaryFormatter bf = new BinaryFormatter();
            return bf.Deserialize(ms);
        }
        #endregion

        #region CloseConnection
        private void CloseConnection()
        {
            try
            {
                 Show();
                WindowState = System.Windows.WindowState.Normal;
                Start_Button.IsEnabled = true;
                localStatus.Content = "";
                _borderWindow.Hide();

                //check if the Server window is closed before the client is connected
                if (_listenerMouse != null)
                {
                    _listenerMouse.Dispose();
                    _listenerMouse.Close();
                }
                if (_handlerMouse != null)
                {
                    _handlerMouse.Dispose();
                    _handlerMouse.Close();
                }
                if (_listenerKeyboard != null)
                {
                    _listenerKeyboard.Dispose();
                    _listenerClipboard.Close();
                }
                if(_handlerKeyboard != null)
                {
                    _handlerKeyboard.Dispose();
                    _handlerKeyboard.Close();
                }
                if (_listenerClipboard != null)
                {
                    _listenerClipboard.Dispose();
                    _listenerKeyboard.Close();
                }
                if(_handlerClipboard != null)
                {
                    _handlerClipboard.Dispose();
                    _handlerClipboard.Close();
                }
                if (_listenerMessage != null)
                {
                    _listenerMessage.Dispose();
                    _listenerMessage.Close();
                }
                if(_handlerMessage != null)
                {
                    _handlerMessage.Dispose();
                    _handlerMessage.Close();
                }
            }
            catch (Exception exc) { OnErrorServer(new MyEventArgs(exc.ToString())); }
        }
        #endregion

        #region Window_Closed
        private void Window_Closed(object sender, EventArgs e)
        {
            DeleteDirectory(@"C:\temp\");
            _borderWindow.Closable = true;
            _borderWindow.Close();
        } 
        #endregion

        #region DeleteDirectory
        public static void DeleteDirectory(string target_dir)
        {
            try
            {
                if (Directory.Exists(target_dir))
                {
                    string[] files = Directory.GetFiles(target_dir);
                    string[] dirs = Directory.GetDirectories(target_dir);

                    foreach (string file in files)
                    {
                        File.SetAttributes(file, FileAttributes.Normal);
                        File.Delete(file);
                    }

                    foreach (string dir in dirs)
                    {
                        DeleteDirectory(dir);
                    }

                    Directory.Delete(target_dir, false);
                }
            }
            catch (Exception)
            {

            }
        }
        #endregion

        #region OnErrorServer
        protected virtual void OnErrorServer(MyEventArgs e)
        {
            if (ErrorServer != null)
                ErrorServer(this, e);
        }
        #endregion

        #region OnUpdateProgressBar
        protected static void OnUpdateProgressBar(MyEventArgs e)
        {
            if (UpdateProgressBar_onBaloon != null)
                UpdateProgressBar_onBaloon(typeof(Server), e);
        }
        #endregion

        #region SendPing
        private void SendPing()
        {
            try {
                while (true)
                {
                    // Send the ping asynchronously.
                    // Use the waiter as the user token.
                    // When the callback completes, it can wake up this thread.
                    PingReply reply = _pingSender.Send(((IPEndPoint)_handlerMessage.RemoteEndPoint).Address, 10000);

                    if (reply.Status == IPStatus.Success)
                    {
                        Thread.Sleep(2000);
                    }
                    else
                    {
                        Debug.Print("Ping failed:");
                        OnErrorServer(new MyEventArgs("Client non raggiungibile"));
                        break;
                    }
                }
            }
            catch (Exception)
            {
                OnErrorServer(new MyEventArgs("Client non raggiungibile"));
            }
        }
        #endregion
    }
}