﻿using System;
using System.Text;
using System.Windows;
using System.Net; 
using System.Net.Sockets;
using ObjectSerializable;
using Control;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Diagnostics;
using ClipboardControl;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Windows.Media.Imaging;
using System.Collections;

namespace ServerSocket
{

    public delegate void GoOnFileEventHandler(object sender, GoOnFileEventArgs e);

    public partial class Server : Window
    {
        private Socket listenerMouse;
        private Socket listenerKeyboard;
        private Socket listenerClipboard;
        private Socket handlerClipboard;

        public Server()
        {
            InitializeComponent();

            Start_Button.IsEnabled = true;

            /********** ICON TRAY *********/
            /* System.Windows.Forms.NotifyIcon ni = new System.Windows.Forms.NotifyIcon();
            ni.Icon = new System.Drawing.Icon("Computers.ico");
            ni.Visible = true;
            ni.DoubleClick +=
                delegate(object sender, EventArgs args)
                {
                    this.Show();
                    this.WindowState = WindowState.Normal;
                };  */
        }

        #region showIconTray -- da vedere
        //protected override void OnStateChanged(EventArgs e)
        //{
        //    if (WindowState == WindowState.Minimized)
        //        this.Hide();

        //    base.OnStateChanged(e);
        //}
        #endregion

        #region startClick
        private void Start_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                /* Creates a network endpoint */
                IPEndPoint ipEndPointM = new IPEndPoint(IPAddress.Any, 4510);
                IPEndPoint ipEndPointK = new IPEndPoint(IPAddress.Any, 4511);
                IPEndPoint ipEndPointC = new IPEndPoint(IPAddress.Any, 4512);

                /* Create one Socket object to listen the incoming connection */
                listenerMouse = new Socket(
                    AddressFamily.InterNetwork,
                    SocketType.Stream,
                    ProtocolType.Tcp
                    );

                listenerKeyboard = new Socket(
                   AddressFamily.InterNetwork,
                   SocketType.Stream,
                   ProtocolType.Tcp
                   );

                listenerClipboard = new Socket(
                   AddressFamily.InterNetwork,
                   SocketType.Stream,
                   ProtocolType.Tcp
                   );

                /* Disable Nagle algorithm */
                listenerMouse.NoDelay = true;
                listenerKeyboard.NoDelay = true;
                listenerClipboard.NoDelay = true;

                /* Associates a Socket with a local endpoint */
                listenerMouse.Bind(ipEndPointM);
                listenerKeyboard.Bind(ipEndPointK);
                listenerClipboard.Bind(ipEndPointC);

                /* Places a Socket in a listening state and specifies the maximum 
                   Length of the pending connections queue */
                listenerMouse.Listen(1);
                listenerKeyboard.Listen(1);
                listenerClipboard.Listen(1);

                /* Begins an asynchronous operation to accept an attempt */
                listenerMouse.BeginAccept(new AsyncCallback(AcceptMouseCallback), listenerMouse);
                listenerKeyboard.BeginAccept(new AsyncCallback(AcceptKeyboardCallback), listenerKeyboard);
                listenerClipboard.BeginAccept(new AsyncCallback(AcceptClipboardCallback), listenerClipboard);

                Start_Button.IsEnabled = false;

            }
            catch (Exception exc) { MessageBox.Show(exc.ToString()); }
        }
        #endregion

        #region AcceptMouseCallback
        public void AcceptMouseCallback(IAsyncResult ar)
        {
            try
            {
                // Get Listening Socket object 
                Socket listener = (Socket)ar.AsyncState;
                // Create a new socket to handle a client
                Socket handler = listener.EndAccept(ar);

                StateMouseObject stateMouse = new StateMouseObject();
                stateMouse.workSocket = handler;
                stateMouse.toReceive = StateMouseObject.BufferSize;

                // Begins to asynchronously receive data 
                handler.BeginReceive(
                    stateMouse.buffer,          // An array of type Byt for received data 
                    0,                          // The zero-based position in the buffer  
                    StateMouseObject.BufferSize,// The number of bytes to receive 
                    SocketFlags.None,           // Specifies send and receive behaviors 
                    new AsyncCallback(ReceiveMouseCallback),//An AsyncCallback delegate 
                    stateMouse            // Specifies infomation for receive operation 
                    );


                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    tbStatus.Text = "Server is now listening with Client: " + IPAddress.Parse(((IPEndPoint)handler.RemoteEndPoint).Address.ToString());
                })); 
            }
            catch (Exception exc) { 
                MessageBox.Show(exc.ToString());
            }
        }
        #endregion

        #region ReceiveMouseCallback
        public void ReceiveMouseCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the handler socket
                // from the asynchronous state object.
                StateMouseObject stateMouseObject = (StateMouseObject)ar.AsyncState;
                Socket handler = stateMouseObject.workSocket;

                // Read data from the client socket. 
                int bytesRead = handler.EndReceive(ar);

                if (bytesRead > 0)
                {
                    stateMouseObject.toReceive -= bytesRead;

                    if (stateMouseObject.toReceive == 0)
                    {
                        if (stateMouseObject.ms != null)
                        {
                            EventMouseReceived(stateMouseObject.ms.GetBuffer());
                        }
                        else
                        {
                            EventMouseReceived(stateMouseObject.buffer);
                        }
                            

                        StateMouseObject newStateMouseObject = new StateMouseObject();
                        newStateMouseObject.workSocket = handler;
                        newStateMouseObject.toReceive = StateMouseObject.BufferSize;

                        // Begins to asynchronously receive data 
                        handler.BeginReceive(
                            newStateMouseObject.buffer,         // An array of type Byte for received data 
                            0,                                  // The zero-based position in the buffer  
                            StateMouseObject.BufferSize,        // The number of bytes to receive 
                            SocketFlags.None,                   // Specifies send and receive behaviors 
                            new AsyncCallback(ReceiveMouseCallback), //An AsyncCallback delegate 
                            newStateMouseObject                 // Specifies infomation for receive operation 
                            );
                    }
                    else
                    {
                        stateMouseObject.ms = new MemoryStream();
                        // Transfer the imcomplete object into the steam.
                        stateMouseObject.ms.Write(stateMouseObject.buffer, 0, bytesRead);
                        // Create a new buffer ready for new incoming bytes.
                        stateMouseObject.buffer = new byte[StateMouseObject.BufferSize];
                        handler.BeginReceive(
                            stateMouseObject.buffer,            // An array of type Byt for received data 
                            0,                                  // The zero-based position in the buffer  
                            StateMouseObject.BufferSize,        // The number of bytes to receive 
                            SocketFlags.None,                   // Specifies send and receive behaviors 
                            new AsyncCallback(ReceiveMouseCallback), //An AsyncCallback delegate 
                            stateMouseObject                    // Specifies infomation for receive operation 
                            );
                    }
                }
                else
                {
                    MessageBox.Show("Connection closed by party");
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        handler.Close();
                        CloseConnection();
                    }));
                }
            }
            catch (Exception exc) 
            { 
                MessageBox.Show(exc.ToString());
            }
        }
        #endregion

        #region EventMouseReceived
        private void EventMouseReceived(byte[] buffer)
        {
            EventMouse eventUman = (EventMouse)DeserializeFromBytes(buffer);
            

            switch (eventUman.typeEvent)
            {
                case EventMouse.MOUSE.MOVE:
                    ControlMouseKeyboard.MouseMove((int)eventUman.PX, (int)eventUman.PY);
                    break;

                case EventMouse.MOUSE.CLICKLEFTDOWN:
                    ControlMouseKeyboard.LeftClickDown((int)eventUman.PX, (int)eventUman.PY);
                    break;

                case EventMouse.MOUSE.CLICKLEFTUP:
                    ControlMouseKeyboard.LeftClickUp((int)eventUman.PX, (int)eventUman.PY);
                    break;
                
                case EventMouse.MOUSE.CLICKRIGHTDOWN:
                    ControlMouseKeyboard.RightClickDown((int)eventUman.PX, (int)eventUman.PY);
                    break;

                case EventMouse.MOUSE.CLICKRIGHTUP:
                    ControlMouseKeyboard.RightClickUp((int)eventUman.PX, (int)eventUman.PY);
                    break;

                case EventMouse.MOUSE.WHEELUP:
                    ControlMouseKeyboard.MouseWheelUp((int)eventUman.PX, (int)eventUman.PY);
                    break;

                case EventMouse.MOUSE.WHEELDOWN:
                    ControlMouseKeyboard.MouseWheelDown((int)eventUman.PX, (int)eventUman.PY);
                    break;
            }

        }
        #endregion

        #region AcceptKeyboardCallback
        public void AcceptKeyboardCallback(IAsyncResult ar)
        {
            try
            {
                // Receiving byte array 
                object buffer = new object();
                // Get Listening Socket object 
                Socket listener = (Socket)ar.AsyncState;
                // Create a new socket to handle a client
                Socket handler = listener.EndAccept(ar);

                StateKeyboardObject stateKeyboard = new StateKeyboardObject();
                stateKeyboard.workSocket = handler;
                stateKeyboard.toReceive = StateKeyboardObject.BufferSize;

                // Begins to asynchronously receive data 
                handler.BeginReceive(
                    stateKeyboard.buffer,          // An array of type Byt for received data 
                    0,                          // The zero-based position in the buffer  
                    StateKeyboardObject.BufferSize,// The number of bytes to receive 
                    SocketFlags.None,           // Specifies send and receive behaviors 
                    new AsyncCallback(ReceiveKeyboardCallback),//An AsyncCallback delegate 
                    stateKeyboard            // Specifies infomation for receive operation 
                    );


                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    tbStatus.Text = "Server is now listening with Client: " + IPAddress.Parse(((IPEndPoint)handler.RemoteEndPoint).Address.ToString());
                }));
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString());
            }
        }
        #endregion

        #region ReceiveKeyboardCallback
        public void ReceiveKeyboardCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the handler socket
                // from the asynchronous state object.
                StateKeyboardObject stateKeyboardObject = (StateKeyboardObject)ar.AsyncState;
                Socket handler = stateKeyboardObject.workSocket;

                // Read data from the client socket. 
                int bytesRead = handler.EndReceive(ar);

                if (bytesRead > 0)
                {
                    stateKeyboardObject.toReceive -= bytesRead;
                    Debug.Print("Recived {0} bytes", bytesRead);

                    if (stateKeyboardObject.toReceive == 0)
                    {
                        StateKeyboardObject newStateKeyboardObject = new StateKeyboardObject();
                        newStateKeyboardObject.workSocket = handler;
                        newStateKeyboardObject.toReceive = StateKeyboardObject.BufferSize;
                        if (stateKeyboardObject.ms != null)
                            EventKeyboardReceived(stateKeyboardObject.ms.GetBuffer());
                        else
                            EventKeyboardReceived(stateKeyboardObject.buffer);

                        // Begins to asynchronously receive data 
                        handler.BeginReceive(
                            newStateKeyboardObject.buffer,         // An array of type Byte for received data 
                            0,                                  // The zero-based position in the buffer  
                            StateKeyboardObject.BufferSize,        // The number of bytes to receive 
                            SocketFlags.None,                   // Specifies send and receive behaviors 
                            new AsyncCallback(ReceiveKeyboardCallback), //An AsyncCallback delegate 
                            newStateKeyboardObject                 // Specifies infomation for receive operation 
                            );
                    }
                    else
                    {
                        stateKeyboardObject.ms = new MemoryStream();
                        // Transfer the imcomplete object into the steam.
                        stateKeyboardObject.ms.Write(stateKeyboardObject.buffer, 0, bytesRead);
                        // Create a new buffer ready for new incoming bytes.
                        stateKeyboardObject.buffer = new byte[StateMouseObject.BufferSize];
                        handler.BeginReceive(
                            stateKeyboardObject.buffer,            // An array of type Byt for received data 
                            0,                                  // The zero-based position in the buffer  
                            StateKeyboardObject.BufferSize,        // The number of bytes to receive 
                            SocketFlags.None,                   // Specifies send and receive behaviors 
                            new AsyncCallback(ReceiveKeyboardCallback), //An AsyncCallback delegate 
                            stateKeyboardObject                    // Specifies infomation for receive operation 
                            );
                    }
                }
                else
                {
                    MessageBox.Show("Connection closed by party");
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        handler.Close();
                        CloseConnection();
                    }));
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString());
            }
        }
        #endregion

        #region EventKeyboardReceived
        private void EventKeyboardReceived(byte[] buffer)
        {
            EventKeyboard eventUman = (EventKeyboard)DeserializeFromBytes(buffer);
            #region CombinazioniNonFunzionanti
            //ALT+INVIO = Visualizza proprietà di un elemento
            #endregion
            switch (eventUman.typeEvent)
            {
                case EventKeyboard.KEYBOARD.KEYDOWN:
                    InputSimulatorKeyboard.KeyDown(eventUman.KEY);
                    break;

                case EventKeyboard.KEYBOARD.KEYUP:
                    InputSimulatorKeyboard.KeyUp(eventUman.KEY);
                    break;
                
            }

        }
        #endregion

        #region AcceptClipboardClipboard
        public void AcceptClipboardCallback(IAsyncResult ar)
        {
            // Get Listening Socket object 
            Socket listener = (Socket)ar.AsyncState;
            // Create a new socket to handle a client
            handlerClipboard = listener.EndAccept(ar);

            Debug.Print("Accept clibboard");


            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                tbStatus.Text = "Server is now listening with Client: " + IPAddress.Parse(((IPEndPoint)handlerClipboard.RemoteEndPoint).Address.ToString());
            }));
        }
        #endregion

        #region DeserializeFromBytes
        private static Object DeserializeFromBytes(byte[] buffer)
        {
            MemoryStream ms = new MemoryStream(buffer);
            BinaryFormatter bf = new BinaryFormatter();
            return bf.Deserialize(ms);
        }
        #endregion

        #region CloseConnection
        private void CloseConnection()
        {
            try
            {
                listenerMouse.Close();
                listenerClipboard.Close();
                listenerKeyboard.Close();
                Start_Button.IsEnabled = true;
                tbStatus.Text = "Server is ready to starting";
            }
            catch (Exception exc) { MessageBox.Show(exc.ToString()); }
        }
        #endregion

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SendClipboard.SendClip(handlerClipboard);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ReceiveClipboard.ReceiveClip(handlerClipboard);
        }

    }
}