﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;

namespace ServerSocket
{
    /// <summary>
    /// Logica di interazione per ServerHighlight.xaml
    /// </summary>
    public partial class BorderWindow : Window
    {
        private bool _closable;

        public bool Closable
        {
            get
            {
                return _closable;
            }

            set
            {
                _closable = value;
            }
        }

        public BorderWindow()
        {
            InitializeComponent();
            _closable = false;
            #region adjustment of margins respect to the taskbar
            double marginBottomCalculated = SystemParameters.PrimaryScreenHeight - SystemParameters.WorkArea.Height;

            Thickness marginBottom = BorderBottom.Margin;
            marginBottom.Bottom = marginBottomCalculated;
            BorderBottom.Margin = marginBottom;

            Thickness marginLeft = BorderLeft.Margin;
            marginLeft.Bottom = marginBottomCalculated+1;
            BorderLeft.Margin = marginLeft;

            Thickness marginRight = BorderRight.Margin;
            marginRight.Bottom = marginBottomCalculated + 1;
            BorderRight.Margin = marginRight;
            #endregion
        }

        #region Make events pass through the Window
        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            var hwnd = new WindowInteropHelper(this).Handle;
            WindowsServices.SetWindowExTransparent(hwnd);
        }
        #endregion

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // If user doesn't want to close, cancel closure
            if(!_closable)
                e.Cancel = true;
        }
    }

    #region WindowsServices
    public static class WindowsServices
    {
        const int WS_EX_TRANSPARENT = 0x00000020;
        const int GWL_EXSTYLE = (-20);

        [DllImport("user32.dll")]
        static extern int GetWindowLong(IntPtr hwnd, int index);

        [DllImport("user32.dll")]
        static extern int SetWindowLong(IntPtr hwnd, int index, int newStyle);

        public static void SetWindowExTransparent(IntPtr hwnd)
        {
            var extendedStyle = GetWindowLong(hwnd, GWL_EXSTYLE);
            SetWindowLong(hwnd, GWL_EXSTYLE, extendedStyle | WS_EX_TRANSPARENT);
        }
    }
    #endregion
}
