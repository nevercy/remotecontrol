﻿using System;
using System.Windows.Input;

namespace IconTray
{
    public class ShowMessageCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        public delegate void WindowHandler(object s, EventArgs e);
        public static event WindowHandler ShowWindowEvent;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            if (parameter.ToString() == "Shutdown")
            {
                if (ShowWindowEvent != null)
                    ShowWindowEvent(this, new IconEventArgs("close"));
            }
            if (parameter.ToString() == "Reactivate.")
            {
                //raise Event To ReOpen The Window
                if (ShowWindowEvent != null)
                    ShowWindowEvent(this, new IconEventArgs("reopen"));
            }
        }

    }

    public class IconEventArgs : System.EventArgs
    {
        string type;

        public IconEventArgs(string v)
        {
            this.type = v;
        }

        public string Type
        {
            get
            {
                return type;
            }

            set
            {
                type = value;
            }
        }
    }
}
