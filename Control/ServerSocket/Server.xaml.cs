﻿using System;
using System.Text;
using System.Windows;
using System.Net;
using System.Net.Sockets;
using ObjectSerializable;
using Control;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Diagnostics;
using ClipboardControl;
using System.Windows.Controls.Primitives; //It is important for the notification on BaloonText
using MahApps.Metro.Controls;
using IconTray;
using Baloon;
using Event;

namespace ServerSocket
{
    public delegate void ServerEventHandler(object sender, MyEventArgs args);
    public partial class Server : MetroWindow
    {
        private Socket listenerMouse;
        private Socket handlerMouse;
        private Socket listenerKeyboard;
        private Socket handlerKeyboard;
        private Socket listenerClipboard;
        private Socket handlerClipboard;
        private Socket listenerMessage;
        private Socket handlerMessage;
        private BaloonText balloon;
        private ServerHighlight borderWindow;

        private event ServerEventHandler ErrorServer;

        #region Init
        public Server()
        {
            InitializeComponent();

            balloon = new BaloonText();
            balloon.ChangeMessage("Welcome");
            //show balloon and close it after 2 seconds
            MyNotifyIcon.ShowCustomBalloon(balloon, PopupAnimation.Slide, 4000);

            borderWindow = new ServerHighlight();
            borderWindow.Hide();
            Start_Button.IsEnabled = true;

            if (!Directory.Exists(@"C:\temp\"))
                Directory.CreateDirectory(@"C:\temp\");

            ShowMessageCommand.ShowWindowEvent += ManageIconTray;
            this.ErrorServer += Server_ErrorServer;
        }
        #endregion

        #region ManageIconTray
        //This code is used to handle the visibility of the window
        //The handle of the event is activated and registered at the startup of the Server
        //How you can see the visibility is changed across the method Hide of the window
        //The event is sended to the .NET by clicking the context Menu on the icon tray
        void ManageIconTray(object s, EventArgs e)
        {
            IconEventArgs iea = (IconEventArgs)e;
            if (iea.Type == "reopen")
                ChangeVisibility();
            else
            {
                CloseConnection(false);
                Close();
            }
        }
        #endregion

        #region ChangeVisibility
        void ChangeVisibility()
        {
            if (Visibility == Visibility.Hidden || this.WindowState == WindowState.Minimized)
            {
                Show();
                this.WindowState = System.Windows.WindowState.Normal;
            }
        }
        #endregion

        #region startClick
        private void Start_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                /* Creates a network endpoint */
                IPEndPoint ipEndPointM = new IPEndPoint(IPAddress.Any, 4510);
                IPEndPoint ipEndPointK = new IPEndPoint(IPAddress.Any, 4511);
                IPEndPoint ipEndPointC = new IPEndPoint(IPAddress.Any, 4512);
                IPEndPoint ipEndPointS = new IPEndPoint(IPAddress.Any, 4513);

                /* Create one Socket object to listen the incoming connection */
                listenerMouse = new Socket(
                    AddressFamily.InterNetwork,
                    SocketType.Stream,
                    ProtocolType.Tcp
                    );

                listenerKeyboard = new Socket(
                   AddressFamily.InterNetwork,
                   SocketType.Stream,
                   ProtocolType.Tcp
                   );

                listenerClipboard = new Socket(
                   AddressFamily.InterNetwork,
                   SocketType.Stream,
                   ProtocolType.Tcp
                   );

                listenerMessage = new Socket(
                   AddressFamily.InterNetwork,
                   SocketType.Stream,
                   ProtocolType.Tcp
                   );

                /* Disable Nagle algorithm */
                listenerMouse.NoDelay = true;
                listenerKeyboard.NoDelay = true;
                listenerClipboard.NoDelay = true;
                listenerMessage.NoDelay = true;

                /* Associates a Socket with a local endpoint */
                listenerMouse.Bind(ipEndPointM);
                listenerKeyboard.Bind(ipEndPointK);
                listenerClipboard.Bind(ipEndPointC);
                listenerMessage.Bind(ipEndPointS);

                /* Places a Socket in a listening state and specifies the maximum 
                   Length of the pending connections queue */
                listenerMouse.Listen(1);
                listenerKeyboard.Listen(1);
                listenerClipboard.Listen(1);
                listenerMessage.Listen(1);

                /* Begins an asynchronous operation to accept an attempt */
                listenerMouse.BeginAccept(new AsyncCallback(AcceptMouseCallback), listenerMouse);
                listenerKeyboard.BeginAccept(new AsyncCallback(AcceptKeyboardCallback), listenerKeyboard);
                listenerClipboard.BeginAccept(new AsyncCallback(AcceptClipboardCallback), listenerClipboard);
                listenerMessage.BeginAccept(new AsyncCallback(AcceptStatusCallback), listenerMessage);

                Start_Button.IsEnabled = false;
                //status.Text = "Server listening at port 4510";
                balloon = new BaloonText();
                balloon.ChangeMessage("Server listening at port 4510");
                //show balloon and close it after 2 seconds
                MyNotifyIcon.ShowCustomBalloon(balloon, PopupAnimation.Slide, 4000);
            }
            catch (Exception exc) { OnErrorServer(new MyEventArgs(exc.ToString())); }
        }
        #endregion

        #region AcceptMouseCallback
        public void AcceptMouseCallback(IAsyncResult ar)
        {
            try
            {
                // Get Listening Socket object 
                Socket listener = (Socket)ar.AsyncState;
                // Create a new socket to handle a client
                handlerMouse = listener.EndAccept(ar);

                StateMouseObject stateMouse = new StateMouseObject();
                stateMouse.workSocket = handlerMouse;
                stateMouse.toReceive = StateMouseObject.BufferSize;

                // Begins to asynchronously receive data 
                handlerMouse.BeginReceive(
                    stateMouse.buffer,          // An array of type Byt for received data 
                    0,                          // The zero-based position in the buffer  
                    StateMouseObject.BufferSize,// The number of bytes to receive 
                    SocketFlags.None,           // Specifies send and receive behaviors 
                    new AsyncCallback(ReceiveMouseCallback),//An AsyncCallback delegate 
                    stateMouse            // Specifies infomation for receive operation 
                    );


                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    Hide(); //Command to disable the visibility of the window , exactly when the connection with the client was established
                    balloon = new BaloonText();
                    balloon.ChangeMessage("Connected with client " + handlerMouse.RemoteEndPoint);
                    MyNotifyIcon.ShowCustomBalloon(balloon, PopupAnimation.Slide, 4000);
                    borderWindow.Show();
                }));
                
            }
            catch (Exception exc) {
                OnErrorServer(new MyEventArgs(exc.ToString()));
            }
        }
        #endregion

        #region ReceiveMouseCallback
        public void ReceiveMouseCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the handler socket
                // from the asynchronous state object.
                StateMouseObject stateMouseObject = (StateMouseObject)ar.AsyncState;
                Socket handler = stateMouseObject.workSocket;

                // Read data from the client socket. 
                int bytesRead = handler.EndReceive(ar);

                if (bytesRead > 0)
                {
                    stateMouseObject.toReceive -= bytesRead;

                    if (stateMouseObject.toReceive == 0)
                    {
                        if (stateMouseObject.ms != null)
                        {
                            EventMouseReceived(stateMouseObject.ms.GetBuffer());
                        }
                        else
                        {
                            EventMouseReceived(stateMouseObject.buffer);
                        }
                            

                        StateMouseObject newStateMouseObject = new StateMouseObject();
                        newStateMouseObject.workSocket = handler;
                        newStateMouseObject.toReceive = StateMouseObject.BufferSize;

                        // Begins to asynchronously receive data 
                        handler.BeginReceive(
                            newStateMouseObject.buffer,         // An array of type Byte for received data 
                            0,                                  // The zero-based position in the buffer  
                            StateMouseObject.BufferSize,        // The number of bytes to receive 
                            SocketFlags.None,                   // Specifies send and receive behaviors 
                            new AsyncCallback(ReceiveMouseCallback), //An AsyncCallback delegate 
                            newStateMouseObject                 // Specifies infomation for receive operation 
                            );
                    }
                    else
                    {
                        stateMouseObject.ms = new MemoryStream();
                        // Transfer the imcomplete object into the steam.
                        stateMouseObject.ms.Write(stateMouseObject.buffer, 0, bytesRead);
                        // Create a new buffer ready for new incoming bytes.
                        stateMouseObject.buffer = new byte[StateMouseObject.BufferSize];
                        handler.BeginReceive(
                            stateMouseObject.buffer,            // An array of type Byt for received data 
                            0,                                  // The zero-based position in the buffer  
                            StateMouseObject.BufferSize,        // The number of bytes to receive 
                            SocketFlags.None,                   // Specifies send and receive behaviors 
                            new AsyncCallback(ReceiveMouseCallback), //An AsyncCallback delegate 
                            stateMouseObject                    // Specifies infomation for receive operation 
                            );
                    }
                }
                else
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        handler.Close();
                        CloseConnection(true);
                    }));
                }
            }
            catch (Exception exc) 
            {
                OnErrorServer(new MyEventArgs(exc.ToString()));
            }
        }
        #endregion

        #region EventMouseReceived
        private void EventMouseReceived(byte[] buffer)
        {
            EventMouse eventUman = (EventMouse)DeserializeFromBytes(buffer);
            

            switch (eventUman.typeEvent)
            {
                case EventMouse.MOUSE.MOVE:
                    
                    ControlMouseKeyboard.MouseMove((int)eventUman.PX, (int)eventUman.PY, (int)eventUman.XScreenResolution, (int)eventUman.YScreenResolution);
                    break;

                case EventMouse.MOUSE.CLICKLEFTDOWN:
                    ControlMouseKeyboard.LeftClickDown((int)eventUman.PX, (int)eventUman.PY, (int)eventUman.XScreenResolution, (int)eventUman.YScreenResolution);
                    break;

                case EventMouse.MOUSE.CLICKLEFTUP:
                    ControlMouseKeyboard.LeftClickUp((int)eventUman.PX, (int)eventUman.PY, (int)eventUman.XScreenResolution, (int)eventUman.YScreenResolution);
                    break;
                
                case EventMouse.MOUSE.CLICKRIGHTDOWN:
                    ControlMouseKeyboard.RightClickDown((int)eventUman.PX, (int)eventUman.PY, (int)eventUman.XScreenResolution, (int)eventUman.YScreenResolution);
                    break;

                case EventMouse.MOUSE.CLICKRIGHTUP:
                    ControlMouseKeyboard.RightClickUp((int)eventUman.PX, (int)eventUman.PY, (int)eventUman.XScreenResolution, (int)eventUman.YScreenResolution);
                    break;

                case EventMouse.MOUSE.WHEELUP:
                    ControlMouseKeyboard.MouseWheelUp((int)eventUman.PX, (int)eventUman.PY, (int)eventUman.XScreenResolution, (int)eventUman.YScreenResolution);
                    break;

                case EventMouse.MOUSE.WHEELDOWN:
                    ControlMouseKeyboard.MouseWheelDown((int)eventUman.PX, (int)eventUman.PY, (int)eventUman.XScreenResolution, (int)eventUman.YScreenResolution);
                    break;
            }

        }
        #endregion

        #region AcceptKeyboardCallback
        public void AcceptKeyboardCallback(IAsyncResult ar)
        {
            try
            {
                // Receiving byte array 
                object buffer = new object();
                // Get Listening Socket object 
                Socket listener = (Socket)ar.AsyncState;
                // Create a new socket to handle a client
                handlerKeyboard = listener.EndAccept(ar);

                StateKeyboardObject stateKeyboard = new StateKeyboardObject();
                stateKeyboard.workSocket = handlerKeyboard;
                stateKeyboard.toReceive = StateKeyboardObject.BufferSize;

                // Begins to asynchronously receive data 
                handlerKeyboard.BeginReceive(
                    stateKeyboard.buffer,          // An array of type Byt for received data 
                    0,                          // The zero-based position in the buffer  
                    StateKeyboardObject.BufferSize,// The number of bytes to receive 
                    SocketFlags.None,           // Specifies send and receive behaviors 
                    new AsyncCallback(ReceiveKeyboardCallback),//An AsyncCallback delegate 
                    stateKeyboard            // Specifies infomation for receive operation 
                    );

            }
            catch (Exception exc)
            {
                OnErrorServer(new MyEventArgs(exc.ToString()));
            }
        }
        #endregion

        #region ReceiveKeyboardCallback
        public void ReceiveKeyboardCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the handler socket
                // from the asynchronous state object.
                StateKeyboardObject stateKeyboardObject = (StateKeyboardObject)ar.AsyncState;
                Socket handler = stateKeyboardObject.workSocket;

                // Read data from the client socket. 
                int bytesRead = handler.EndReceive(ar);

                if (bytesRead > 0)
                {
                    stateKeyboardObject.toReceive -= bytesRead;
                    Debug.Print("Recived {0} bytes", bytesRead);

                    if (stateKeyboardObject.toReceive == 0)
                    {
                        StateKeyboardObject newStateKeyboardObject = new StateKeyboardObject();
                        newStateKeyboardObject.workSocket = handler;
                        newStateKeyboardObject.toReceive = StateKeyboardObject.BufferSize;
                        if (stateKeyboardObject.ms != null)
                            EventKeyboardReceived(stateKeyboardObject.ms.GetBuffer());
                        else
                            EventKeyboardReceived(stateKeyboardObject.buffer);

                        // Begins to asynchronously receive data 
                        handler.BeginReceive(
                            newStateKeyboardObject.buffer,         // An array of type Byte for received data 
                            0,                                  // The zero-based position in the buffer  
                            StateKeyboardObject.BufferSize,        // The number of bytes to receive 
                            SocketFlags.None,                   // Specifies send and receive behaviors 
                            new AsyncCallback(ReceiveKeyboardCallback), //An AsyncCallback delegate 
                            newStateKeyboardObject                 // Specifies infomation for receive operation 
                            );
                    }
                    else
                    {
                        stateKeyboardObject.ms = new MemoryStream();
                        // Transfer the imcomplete object into the steam.
                        stateKeyboardObject.ms.Write(stateKeyboardObject.buffer, 0, bytesRead);
                        // Create a new buffer ready for new incoming bytes.
                        stateKeyboardObject.buffer = new byte[StateMouseObject.BufferSize];
                        handler.BeginReceive(
                            stateKeyboardObject.buffer,            // An array of type Byt for received data 
                            0,                                  // The zero-based position in the buffer  
                            StateKeyboardObject.BufferSize,        // The number of bytes to receive 
                            SocketFlags.None,                   // Specifies send and receive behaviors 
                            new AsyncCallback(ReceiveKeyboardCallback), //An AsyncCallback delegate 
                            stateKeyboardObject                    // Specifies infomation for receive operation 
                            );
                    }
                }
                else
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        handler.Close();
                    }));
                }
            }
            catch (Exception exc)
            {
                OnErrorServer(new MyEventArgs(exc.ToString()));
            }
        }
        #endregion

        #region EventKeyboardReceived
        private void EventKeyboardReceived(byte[] buffer)
        {
            EventKeyboard eventUman = (EventKeyboard)DeserializeFromBytes(buffer);
            #region CombinazioniNonFunzionanti
            //ALT+INVIO = Visualizza proprietà di un elemento
            #endregion
            switch (eventUman.typeEvent)
            {
                case EventKeyboard.KEYBOARD.KEYDOWN:
                    InputSimulatorKeyboard.KeyDown(eventUman.KEY);
                    break;

                case EventKeyboard.KEYBOARD.KEYUP:
                    InputSimulatorKeyboard.KeyUp(eventUman.KEY);
                    break;
                case EventKeyboard.KEYBOARD.SHORTCUT:
                    Debug.Print("Ricevuto shortcut");
                    ReceiveClipboard.ReceiveClip(handlerClipboard);
                    break;
            }

        }
        #endregion

        #region AcceptClipboardCallback
        public void AcceptClipboardCallback(IAsyncResult ar)
        {
            // Get Listening Socket object 
            Socket listener = (Socket)ar.AsyncState;
            // Create a new socket to handle a client
            handlerClipboard = listener.EndAccept(ar);
        }
        #endregion

        #region AcceptStatusCallback
        public void AcceptStatusCallback(IAsyncResult ar)
        {
            // Get Listening Socket object 
            Socket listener = (Socket)ar.AsyncState;
            // Create a new socket to handle a client
            handlerMessage = listener.EndAccept(ar);

            StateStatusObject stateStatus = new StateStatusObject();
            stateStatus.workSocket = handlerMessage;
            stateStatus.toReceive = StateStatusObject.BufferSize;

            // Begins to asynchronously receive data 
            handlerMessage.BeginReceive(
                stateStatus.buffer,          // An array of type Byt for received data 
                0,                          // The zero-based position in the buffer  
                StateStatusObject.BufferSize,// The number of bytes to receive 
                SocketFlags.None,           // Specifies send and receive behaviors 
                new AsyncCallback(ReceiveStatusCallback),//An AsyncCallback delegate 
                stateStatus            // Specifies infomation for receive operation 
                );
        }
        #endregion

        #region ReceiveStatusCallback
        public void ReceiveStatusCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the handler socket
                // from the asynchronous state object.
                StateStatusObject stateStatusObject = (StateStatusObject)ar.AsyncState;
                Socket handler = stateStatusObject.workSocket;

                // Read data from the client socket. 
                int bytesRead = handler.EndReceive(ar);

                if (bytesRead > 0)
                {
                    //stateStatusObject.toReceive -= bytesRead;
                    stateStatusObject.toReceive -= 256;
                    Debug.Print("STATUS: Recived {0} bytes", bytesRead);

                    if (stateStatusObject.toReceive == 0)
                    {
                        Debug.Print("STATUS: Da ricevere {0} bytes", stateStatusObject.toReceive);
                        StateStatusObject newStateStatusObject = new StateStatusObject();
                        newStateStatusObject.workSocket = handler;
                        newStateStatusObject.toReceive = StateStatusObject.BufferSize;

                        if (stateStatusObject.ms != null)
                            EventStatusReceived(stateStatusObject.ms.GetBuffer(), 256);
                        else
                            EventStatusReceived(stateStatusObject.buffer, bytesRead);

                        // Begins to asynchronously receive data 
                        handler.BeginReceive(
                            newStateStatusObject.buffer,         // An array of type Byte for received data 
                            0,                                  // The zero-based position in the buffer  
                            StateStatusObject.BufferSize,        // The number of bytes to receive 
                            SocketFlags.None,                   // Specifies send and receive behaviors 
                            new AsyncCallback(ReceiveStatusCallback), //An AsyncCallback delegate 
                            newStateStatusObject                 // Specifies infomation for receive operation 
                            );
                    }
                    else
                    {
                        Debug.Print("STATUS: Da ricevere {0} bytes", stateStatusObject.toReceive);
                        stateStatusObject.ms = new MemoryStream();
                        // Transfer the imcomplete object into the steam.
                        stateStatusObject.ms.Write(stateStatusObject.buffer, 0, bytesRead);
                        // Create a new buffer ready for new incoming bytes.
                        stateStatusObject.buffer = new byte[StateStatusObject.BufferSize];
                        handler.BeginReceive(
                            stateStatusObject.buffer,            // An array of type Byt for received data 
                            0,                                  // The zero-based position in the buffer  
                            StateStatusObject.BufferSize,        // The number of bytes to receive 
                            SocketFlags.None,                   // Specifies send and receive behaviors 
                            new AsyncCallback(ReceiveStatusCallback), //An AsyncCallback delegate 
                            stateStatusObject                    // Specifies infomation for receive operation 
                            );
                    }
                }
                else
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        handler.Close();
                    }));
                }
            }
            catch (Exception exc)
            {
                OnErrorServer(new MyEventArgs(exc.ToString()));
            }
        }
        #endregion

        #region EventStatusReceived
        private void EventStatusReceived(byte[] data, int dataRead)
        {
            String content = String.Empty;
            StringBuilder sb = new StringBuilder();
            sb.Append(Encoding.ASCII.GetString(data, 0, dataRead));

            Debug.Print("STATUS: Event status received");
            content = sb.ToString();
            Debug.Print("STATUS: content" + content);

            switch (content)
            {
                case "fine invio":
                    ReceiveClipboard.ErrorReceive += ReceiveClipboard_ErrorReceive;
                    ReceiveClipboard.FinishReceive += ReceiveClipboard_FinishReceive;
                    ReceiveClipboard.ReceiveClip(handlerClipboard);
                    Debug.Print("STATUS: "+content);
                    break;
                case "remote copy":
                    SendClipboard.ErrorSend += SendClipboard_ErrorSend;     // raised if there's an problem to send.
                    SendClipboard.FinishSend += SendClipboard_FinishSend;   //raised when the all content of the clipboard is sent.
                    SendClipboard.SendClip(handlerClipboard, handlerMessage);
                    break;
            };
        }
        #endregion

        #region Handlers "ReceiveClipboard"
        /* The two event are rised inside SendClipbord class 
        * and the handlers are added inside KeyboardDown method. */
        void ReceiveClipboard_FinishReceive(object sender, MyEventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                balloon = new BaloonText();
                balloon.ChangeMessage(e.Message);
                MyNotifyIcon.ShowCustomBalloon(balloon, PopupAnimation.Slide, 4000);
            }));

            ReceiveClipboard.ErrorReceive -= ReceiveClipboard_ErrorReceive;
            ReceiveClipboard.FinishReceive -= ReceiveClipboard_FinishReceive;

        }

        void ReceiveClipboard_ErrorReceive(object sender, MyEventArgs e)
        { 
            ReceiveClipboard.ErrorReceive -= ReceiveClipboard_ErrorReceive;
            ReceiveClipboard.FinishReceive -= ReceiveClipboard_FinishReceive;
            OnErrorServer(e);
        }  
        #endregion

        #region Handlers "SendClipboard"
        /* The two event are rised inside SendClipbord class 
         * and the handlers are added inside KeyboardDown method. */
        void SendClipboard_FinishSend(object sender, MyEventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                balloon = new BaloonText();
                balloon.ChangeMessage(e.Message);
                MyNotifyIcon.ShowCustomBalloon(balloon, PopupAnimation.Slide, 4000);
            }));
            /* It's possible disable the handler */
            SendClipboard.ErrorSend -= SendClipboard_ErrorSend;
            SendClipboard.FinishSend -= SendClipboard_FinishSend;

        }

        void SendClipboard_ErrorSend(object sender, MyEventArgs e)
        {

            SendClipboard.ErrorSend -= SendClipboard_ErrorSend;
            SendClipboard.FinishSend -= SendClipboard_FinishSend;

            OnErrorServer(e);
        }
        #endregion

        #region Handler ErrorServer
        private void Server_ErrorServer(object sender, MyEventArgs args)
        {
            this.ErrorServer -= Server_ErrorServer;
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                balloon = new BaloonText();
                balloon.ChangeMessage("Si è verificato un problema. Connessione chiusa col Client");
                MyNotifyIcon.ShowCustomBalloon(balloon, PopupAnimation.Slide, 4000);
            }));

            CloseConnection(false);
        }
        #endregion

        #region DeserializeFromBytes
        private static Object DeserializeFromBytes(byte[] buffer)
        {
            MemoryStream ms = new MemoryStream(buffer);
            BinaryFormatter bf = new BinaryFormatter();
            return bf.Deserialize(ms);
        }
        #endregion

        #region CloseConnection
        private void CloseConnection(bool party)
        {
            try
            {
                // if click "Esci" from contexMenu, don't show Window
                if (party) 
                {
                    balloon = new BaloonText();
                    balloon.ChangeMessage("Connection closed by party");
                    MyNotifyIcon.ShowCustomBalloon(balloon, PopupAnimation.Slide, 4000);
                }
                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    Show();
                    WindowState = System.Windows.WindowState.Normal;
                    Start_Button.IsEnabled = true;
                    status.Text = "Server is ready to starting";
                    borderWindow.Hide();
                }));

                //check if the Server window is closed before the client is connected
                if (listenerMouse != null)
                {
                    listenerMouse.Dispose();
                    listenerMouse.Close();
                }
                if (handlerMouse != null)
                {
                    handlerMouse.Dispose();
                    handlerMouse.Close();
                }
                if (listenerKeyboard != null)
                {
                    listenerKeyboard.Dispose();
                    listenerClipboard.Close();
                }
                if(handlerKeyboard != null)
                {
                    handlerKeyboard.Dispose();
                    handlerKeyboard.Close();
                }
                if (listenerClipboard != null)
                {
                    listenerClipboard.Dispose();
                    listenerKeyboard.Close();
                }
                if(handlerClipboard != null)
                {
                    handlerClipboard.Dispose();
                    handlerClipboard.Close();
                }
                if (listenerMessage != null)
                {
                    listenerMessage.Dispose();
                    listenerMessage.Close();
                }
                if(handlerMessage != null)
                {
                    handlerMessage.Dispose();
                    handlerMessage.Close();
                }

            }
            catch (Exception exc) { OnErrorServer(new MyEventArgs(exc.ToString())); }
        }
        #endregion

        #region Window_Closed
        private void Window_Closed(object sender, EventArgs e)
        {
            DeleteDirectory(@"C:\temp\");
            borderWindow.Close();
        } 
        #endregion

        #region DeleteDirectory
        public static void DeleteDirectory(string target_dir)
        {
            if (Directory.Exists(target_dir))
            {
                string[] files = Directory.GetFiles(target_dir);
                string[] dirs = Directory.GetDirectories(target_dir);

                foreach (string file in files)
                {
                    File.SetAttributes(file, FileAttributes.Normal);
                    File.Delete(file);
                }

                foreach (string dir in dirs)
                {
                    DeleteDirectory(dir);
                }

                Directory.Delete(target_dir, false);
            }
        }
        #endregion

        #region OnErrorServer
        protected virtual void OnErrorServer(MyEventArgs e)
        {
            if (ErrorServer != null)
                ErrorServer(this, e);
        }
        #endregion
    }
}