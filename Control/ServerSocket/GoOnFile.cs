﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjectSerializable;

namespace ServerSocket
{
    public class GoOnFileEventArgs : System.EventArgs
    {
        private int indexList = 0;

        private StateClipboardObject stateClipboardObject;

        public StateClipboardObject StateClipboardObject
        {
            get { return stateClipboardObject; }
            set { stateClipboardObject = value; }
        }

        public int IndexList
        {
            get { return indexList; }
            set { indexList = value; }
        }

        public GoOnFileEventArgs(StateClipboardObject state)
        {
            stateClipboardObject = state;
        }

        public void IncrementIndex()
        {
            indexList++;
        }
    }
}
