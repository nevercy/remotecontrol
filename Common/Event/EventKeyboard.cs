﻿using System;
using System.Runtime.Serialization;
using System.Windows.Input;


namespace ObjectSerializable
{
    [Serializable]
    public class EventKeyboard : ISerializable
    {
        public enum KEYBOARD {
            KEYDOWN,
            KEYUP,
            REMOTECOPY,
            REMOTEPAST
        };

        private int key;
        public KEYBOARD typeEvent;

        public int KEY
        {
            get { return key; }
            set { key = value; }
        }

        public EventKeyboard() { }

        public EventKeyboard(KEYBOARD type, int k)
        {
            this.typeEvent = type;
            this.key = k;
        }

        public EventKeyboard(KEYBOARD type)
        {
            this.typeEvent = type;
        }

        //Deserialization constructor.
        public EventKeyboard(SerializationInfo info, StreamingContext ctxt)
        {
            //Get the values from info and assign them to the appropriate properties
            String eventString = (String)info.GetValue("Event", typeof(String));
            // Parse string to enum
            this.typeEvent = (KEYBOARD)Enum.Parse(typeof(KEYBOARD), eventString, true);
            this.key = (int)info.GetValue("Key", typeof(int));
        }

        //Serialization function.
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            //You can use any custom name for your name-value pair. But make sure you
            // read the values with the same name. 
            info.AddValue("Event", this.typeEvent.ToString());
            info.AddValue("Key", this.key.ToString());
        }
    }
}
