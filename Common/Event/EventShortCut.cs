﻿using System;
using System.Runtime.Serialization;
using System.Windows.Input;


namespace ObjectSerializable
{
    [Serializable]
    class EventShortCut : ISerializable
    {
        public enum SHORTCUT
        {
            CTRL_X,
            CTRL_C,
            CTRL_V
        };

        private int key;
        public SHORTCUT typeEvent;

        public int KEY
        {
            get { return key; }
            set { key = value; }
        }

        public EventShortCut() { }

        public EventShortCut(SHORTCUT type, int k)
        {
            this.typeEvent = type;
            this.key = k;
        }
        //Deserialization constructor.
        public EventShortCut(SerializationInfo info, StreamingContext ctxt)
        {
            //Get the values from info and assign them to the appropriate properties
            String eventString = (String)info.GetValue("Event", typeof(String));
            // Parse string to enum
            this.typeEvent = (SHORTCUT)Enum.Parse(typeof(SHORTCUT), eventString, true);
            this.key = (int)info.GetValue("Key", typeof(int));
        }

        //Serialization function.
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            //You can use any custom name for your name-value pair. But make sure you
            // read the values with the same name. 
            info.AddValue("Event", this.typeEvent.ToString());
            info.AddValue("Key", this.key.ToString());
        }
    }
}
