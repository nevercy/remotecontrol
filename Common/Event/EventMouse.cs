﻿using System;
using System.Runtime.Serialization;


namespace ObjectSerializable
{
    [Serializable]
    public class EventMouse : ISerializable
    {

        public enum MOUSE {
            MOVE,
            CLICKLEFTDOWN,
            CLICKLEFTUP,
            CLICKRIGHTDOWN,
            CLICKRIGHTUP,
            WHEELUP,
            WHEELDOWN
        };


        private double px;
        private double py;
        public MOUSE typeEvent;
        private int xScreenResolution;
        private int yScreenResolution;

        public double PX
        {
            get { return px; }
            set { px = value; }
        }

        public double PY
        {
            get { return py; }
            set { py = value; }
        }

        public int XScreenResolution
        {
            get { return xScreenResolution; }
            set { xScreenResolution = value; }
        }

        public int YScreenResolution
        {
            get { return yScreenResolution; }
            set { yScreenResolution = value; }
        }

        public EventMouse() { }

        public EventMouse(MOUSE type, double x, double y,int SXR,int SYR)
        {
            this.typeEvent = type;
            this.px = x;
            this.py = y;
            this.xScreenResolution = SXR;
            this.yScreenResolution = SYR;
        }

        //Deserialization constructor.
        public EventMouse(SerializationInfo info, StreamingContext ctxt)
        {
            //Get the values from info and assign them to the appropriate properties
            String eventString = (String)info.GetValue("Event", typeof(String));
            // Parse string to enum
            this.typeEvent = (MOUSE)Enum.Parse(typeof(MOUSE), eventString, true);
            this.px = (double)info.GetValue("PositionX", typeof(double));
            this.py = (double)info.GetValue("PositionY", typeof(double));
            this.xScreenResolution = (int)info.GetValue("ResolutionX",typeof(int));
            this.yScreenResolution = (int)info.GetValue("ResolutionY", typeof(int));
        }

        //Serialization function.
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            //You can use any custom name for your name-value pair. But make sure you
            // read the values with the same name. 
            info.AddValue("Event", this.typeEvent.ToString());
            info.AddValue("PositionX", this.px);
            info.AddValue("PositionY", this.py);
            info.AddValue("ResolutionX", this.xScreenResolution);
            info.AddValue("ResolutionY", this.yScreenResolution);
        }

    }
}
