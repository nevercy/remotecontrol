﻿using System;

namespace Event
{
    public class MyEventArgs : EventArgs
    {
        private readonly String message;
        private double valueProgressBar;
        private string _numClient = "";

        public String Message
        {
            get { return message; }
        }

        public double ValueProgressBar
        {
            get
            {
                return valueProgressBar;
            }
        }

        public string NumClient
        {
            get
            {
                return _numClient;
            }

            set
            {
                _numClient = value;
            }
        }

        public MyEventArgs() { }


        public MyEventArgs(String m)
        {
            message = m;
        }

        public MyEventArgs(String m, string numClient)
        {
            message = m;
            NumClient = numClient;
        }

        public MyEventArgs(double d)
        {
            valueProgressBar = d;
        }
    }
}
