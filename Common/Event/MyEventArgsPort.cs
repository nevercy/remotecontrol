﻿using Event;
using System;

namespace Event
{
    public class MyEventArgsPort : MyEventArgs
    {
        private int _portMouse;
        private int _portKeyboard;
        private int _portClipboard;

        public int PortMouse
        {
            get
            {
                return _portMouse;
            }

            set
            {
                _portMouse = value;
            }
        }

        public int PortKeyboard
        {
            get
            {
                return _portKeyboard;
            }

            set
            {
                _portKeyboard = value;
            }
        }

        public int PortClipboard
        {
            get
            {
                return _portClipboard;
            }

            set
            {
                _portClipboard = value;
            }
        }

        public MyEventArgsPort(String m) : base(m)
        {
        }
    }
}
