﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace ObjectSerializable
{
    [Serializable]
    public class ClipboardItem : ISerializable
    {
        private readonly String _type;

        public String Type
        {
            get { return _type; }
        }

        public ClipboardItem(String t)
        {
            if (t == null)
                throw new ArgumentNullException("type null");
            _type = t;
        }

        //Deserialization constructor.
        public ClipboardItem(SerializationInfo info, StreamingContext ctxt)
        {
            if (info == null)
                throw new ArgumentNullException("info null");
            _type = (String)info.GetString("_type");
        }

        //Serialization function.
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected virtual void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            //You can use any custom name for your name-value pair. But make sure you
            // read the values with the same name.
            info.AddValue("_type", _type);
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException("info");

            GetObjectData(info, context);
        }
    }
}
