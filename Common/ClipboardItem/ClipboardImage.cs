﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;


namespace ObjectSerializable
{
    [Serializable]
    public class ClipboardImage : ClipboardItem
    {
        private readonly MemoryStream _image;

        public MemoryStream Image
        {
            get { return _image; }
        }

        public ClipboardImage(String type, MemoryStream ms) : base(type)
        {
            _image = ms;
        }

         //Deserialization constructor.
        protected ClipboardImage(SerializationInfo info, StreamingContext ctxt) : base(info, ctxt)
        {
            //Get the values from info and assign them to the appropriate properties
            _image = (MemoryStream)info.GetValue("Image", typeof(MemoryStream));
        }
        
        //Serialization function.
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            //You can use any custom name for your name-value pair. But make sure you
            // read the values with the same name.
            
            info.AddValue("Image", _image);
            base.GetObjectData(info, ctxt);
        }
    }
}
