﻿using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Security.Permissions;


namespace ObjectSerializable
{
    [Serializable]
    public class ClipboardText : ClipboardItem
    {
        private readonly ArrayList _data;

        public ArrayList Data
        {
            get { return _data; }
        }

        public ClipboardText(String type, ArrayList dO): base(type)
        {
            _data = dO;
        }

        //Deserialization constructor.
        protected ClipboardText(SerializationInfo info, StreamingContext ctxt) :base(info, ctxt)
        {
            //Get the values from info and assign them to the appropriate properties
            _data = (ArrayList)info.GetValue("_data", typeof(ArrayList));
        }
        
        //Serialization function.
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            //You can use any custom name for your name-value pair. But make sure you
            // read the values with the same name.
            info.AddValue("_data", _data);
            base.GetObjectData(info, ctxt);
        }
    }
}
