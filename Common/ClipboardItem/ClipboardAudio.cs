﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;


namespace ObjectSerializable
{
    [Serializable]
    public class ClipboardAudio : ClipboardItem
    {
        private readonly MemoryStream _audio;

        public MemoryStream Audio
        {
            get { return _audio; }
        }

        public ClipboardAudio(String type, MemoryStream ms) : base(type)
        {
            _audio = ms;
        }

        //Deserialization constructor.
        protected ClipboardAudio(SerializationInfo info, StreamingContext ctxt) : base(info, ctxt)
        {
            //Get the values from info and assign them to the appropriate properties
            _audio = (MemoryStream)info.GetValue("Audio", typeof(MemoryStream));
        }
        
        //Serialization function.
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            //You can use any custom name for your name-value pair. But make sure you
            // read the values with the same name.
            
            info.AddValue("Audio", _audio);
            base.GetObjectData(info, ctxt);
        }
    }
}
