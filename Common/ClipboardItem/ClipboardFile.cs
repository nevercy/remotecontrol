﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;


namespace ObjectSerializable
{
    [Serializable]
    public class ClipboardFile : ISerializable
    {
        private readonly String _name;
        private readonly long _fileLen;
        private readonly String _directory;
        private readonly String _fullName;

        public String Name
        {
            get { return _name; }
        }
        
        public long FileLen
        {
            get { return _fileLen; }
        }
        
        public String Directory
        {
            get { return _directory; }
        }
        
        public String FullName
        {
            get { return _fullName; }
        }


        //Public costructor
        public ClipboardFile(String n, long fl, String d, String fn)
        {
            _name = n;
            _fileLen = fl;
            _directory = d;
            _fullName = fn;
        }

        //Deserialization constructor.
        public ClipboardFile(SerializationInfo info, StreamingContext ctxt)
        {
            //Get the values from info and assign them to the appropriate properties
            _name = (String)info.GetValue("NameFile", typeof(String));
            _fileLen = (long)info.GetValue("FileLen", typeof(long));
            _directory = (String)info.GetValue("Directory", typeof(String));
            _fullName = (String)info.GetValue("FullName", typeof(String));
        }

        //Serialization function.
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected virtual void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            //You can use any custom name for your name-value pair. But make sure you
            // read the values with the same name.
            info.AddValue("NameFile", _name);
            info.AddValue("FileLen", _fileLen.ToString());
            info.AddValue("Directory", _directory);
            info.AddValue("FullName", _fullName);
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException("info");

            GetObjectData(info, context);
        }
    }
}
