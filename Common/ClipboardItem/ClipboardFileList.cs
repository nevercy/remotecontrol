﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace ObjectSerializable
{
    [Serializable]
    public class ClipboardFileList : ClipboardItem
    {
        private readonly List<ClipboardFile> _fileList;
        private readonly StringCollection _clipboardList;

        public List<ClipboardFile> FileList
        {
            get { return _fileList; }
        }

        public StringCollection ClipboardList
        {
            get { return _clipboardList; }
        }

        public ClipboardFileList(String type, List<ClipboardFile> fl, StringCollection cl) : base(type)
        {
            _fileList = fl;
            _clipboardList = cl;
        }

        //Deserialization constructor.
        protected ClipboardFileList(SerializationInfo info, StreamingContext ctxt) : base(info, ctxt)
        {
            //Get the values from info and assign them to the appropriate properties
            _fileList = (List<ClipboardFile>)info.GetValue("FileList", typeof(List<ClipboardFile>));
            _clipboardList = (StringCollection)info.GetValue("ClipboardList", typeof(StringCollection));
        }
        
        //Serialization function.
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            //You can use any custom name for your name-value pair. But make sure you
            // read the values with the same name.
            info.AddValue("FileList", _fileList);
            info.AddValue("ClipboardList", _clipboardList);
            base.GetObjectData(info, ctxt);
        }
    }
}
