﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;

namespace ObjectSerializable
{
    public class StateMouseObject
    {
        // Client socket.
        public Socket workSocket = null;
        // Size of receive buffer.
        public const int BufferSize = 256; //256 perchè il MemoryStream è settato come capacità di default 
        // Receive buffer.
        public byte[] buffer = new byte[BufferSize];
        // Total bytes still to be received.
        public int toReceive;
        //Stream of incoming object
       public  MemoryStream ms = null;
    }

    public class StateKeyboardObject
    {
        // Client socket.
        public Socket workSocket = null;
        // Size of receive buffer.
        public const int BufferSize = 256;  //256 perchè il MemoryStream è settato come capacità di default 
        // Receive buffer.
        public byte[] buffer = new byte[BufferSize];
        // Total bytes still to be received.
        public int toReceive;
        //Stream of incoming object
        public MemoryStream ms = null;
    }

    public class StateStatusObject
    {
        // Client socket.
        public Socket workSocket = null;
        // Size of receive buffer.
        public const int BufferSize = 256; //256 perchè il MemoryStream è settato come capacità di default 
        // Receive buffer.
        public byte[] buffer = new byte[BufferSize];
        // Total bytes still to be received.
        public int toReceive;
        //Stream of incoming object
        public MemoryStream ms = null;
        // Received data string.
        public StringBuilder sb = new StringBuilder(); 
    }

    public class StateClipboardObject
    {
        // Client socket.
        //public Socket workSocket = null;
        // Size of receive buffer.
        //public const int BufferSize = 300000;  //47:18
        // Receive buffer.
        public byte[] buffer;

        // 4 bytes perchè la lunghezza è su 32 bit
        public byte[] objLen = new byte[4];
        // Total bytes still to be received.
        public long toReceive;
        public long totalReceived;
        public long fileSize;
        public int indexFile;
        public int bytesRead;

        //Stream of incoming object
        public MemoryStream ms = null;

        public int numBytesChunkReceived = 0;

        public FileStream fileStream = null;

        public ClipboardFile file = null;

        public List<ClipboardFile> fileList = null;

    }

    public class StateClipboardObjectSender
    {
        /// <summary>
        /// Client socket.
        /// </summary>
        //public Socket workSocket = null;
        // Size of receive buffer.
        public const int BufferSize = 300000;  //47:18
        // Receive buffer.
        public byte[] buffer;
        // Total bytes still to be received.
        public int toSend;
        //Stream of incoming object
        public MemoryStream ms = null;

        public Boolean filePresent = false;

        public String fileName = null;

        public string type = null;

        public List<ClipboardFile> fileList = new List<ClipboardFile>();
    }

   
}
